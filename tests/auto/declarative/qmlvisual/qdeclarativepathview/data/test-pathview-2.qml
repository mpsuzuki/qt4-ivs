import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "test-pathview-2.0.png"
    }
    Frame {
        msec: 32
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 48
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 64
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 80
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 96
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 112
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 128
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 144
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 160
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 176
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 192
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 208
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 224
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 240
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 256
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 272
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 288
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 304
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 320
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 336
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 352
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 368
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 384
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 400
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 416
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 432
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 448
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 464
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 480
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 496
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 512
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 528
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 544
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 560
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 576
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 592
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 608
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 624
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 640
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 656
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 672
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 688
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 260; y: 189
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 704
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Frame {
        msec: 720
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 261; y: 188
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 736
        hash: "fe6d7d28dbeef3cfbac3ac3c3e909216"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 262; y: 188
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 266; y: 186
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 752
        hash: "e21cac055208e47e267ac906c7c2ca9c"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 283; y: 183
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 768
        hash: "131e094a79edbeea9a1b981592e55abf"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 302; y: 181
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 331; y: 181
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 784
        hash: "73faabf52bd2af8d8b9d28ce21e5e77b"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 384; y: 179
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 800
        hash: "359554a95362db1734f606cf677001fc"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 432; y: 175
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 432; y: 175
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 816
        hash: "8ef4ecc5c5ba578f0279dc57a6c17ccd"
    }
    Frame {
        msec: 832
        hash: "69c3d9d2700dd395b656b0b09fa63511"
    }
    Frame {
        msec: 848
        hash: "2bbcc36d72c3e9a4b672a46f2aae5076"
    }
    Frame {
        msec: 864
        hash: "125a5f0c8efdf97676edbe379660dcce"
    }
    Frame {
        msec: 880
        hash: "4347a02227207fbf870b6aed76131619"
    }
    Frame {
        msec: 896
        hash: "e08b494c818669bfc48273598574d22e"
    }
    Frame {
        msec: 912
        hash: "186cb5465f45c0df8082ec8cad6ee8b1"
    }
    Frame {
        msec: 928
        hash: "91d04d4469492c3bb2a1ed415dcd904c"
    }
    Frame {
        msec: 944
        hash: "8cc8ef251d68af926a8f300b8666ecfd"
    }
    Frame {
        msec: 960
        hash: "42f64722245f8519386e75ce7e3c0cd9"
    }
    Frame {
        msec: 976
        image: "test-pathview-2.1.png"
    }
    Frame {
        msec: 992
        hash: "058311da9dcf73a4b4928038334b04b5"
    }
    Frame {
        msec: 1008
        hash: "ea662934ee0c3c8d4dbde3ad49448922"
    }
    Frame {
        msec: 1024
        hash: "01991a871819e7bdbf817580f720ead6"
    }
    Frame {
        msec: 1040
        hash: "69a7fe47ae589bcc2607cc42fcea7451"
    }
    Frame {
        msec: 1056
        hash: "8240d087b767311e00b7dd4b8726246c"
    }
    Frame {
        msec: 1072
        hash: "cc70c8e79d68f09e6db0dd43b99906b7"
    }
    Frame {
        msec: 1088
        hash: "2bfabef74bc6e1dbf72111838a0e7557"
    }
    Frame {
        msec: 1104
        hash: "66616f01553364c5bd589b781e22163a"
    }
    Frame {
        msec: 1120
        hash: "58b9de84ebdaabee3917608f2af3bbdb"
    }
    Frame {
        msec: 1136
        hash: "964d96b9b783efb1053501f8a6931248"
    }
    Frame {
        msec: 1152
        hash: "055b77b921a2bac71b6780ab3179f19f"
    }
    Frame {
        msec: 1168
        hash: "074904f31b4f7cf0679f0bf7bba30af2"
    }
    Frame {
        msec: 1184
        hash: "f020a490b6800d5b4402ecb9a8bcd436"
    }
    Frame {
        msec: 1200
        hash: "1615bdedf92f91f089e494d893840c4b"
    }
    Frame {
        msec: 1216
        hash: "b6892f6a5db6d211f0d1bb2bbe5045bf"
    }
    Frame {
        msec: 1232
        hash: "5f0d903ba682923ac69454026a359ed9"
    }
    Frame {
        msec: 1248
        hash: "da5bae496a9ad28585151f4c75ee0c9f"
    }
    Frame {
        msec: 1264
        hash: "68f553248f7ca116671782d1c357b552"
    }
    Frame {
        msec: 1280
        hash: "5503df04dd7f4c88314f9d309a5b36b4"
    }
    Frame {
        msec: 1296
        hash: "cc48c1f58b553adcb27d60f176e2b910"
    }
    Frame {
        msec: 1312
        hash: "661f546199d8753a7b6f6ccea5928c12"
    }
    Frame {
        msec: 1328
        hash: "0fd70052c100f77bddbad177d9e5573d"
    }
    Frame {
        msec: 1344
        hash: "488e0652c0ed82a014de63a64145c34c"
    }
    Frame {
        msec: 1360
        hash: "8b6bf2519080a6e4a61fe216f72dfa09"
    }
    Frame {
        msec: 1376
        hash: "4dab1827f6ce9561297fce8e067df1bd"
    }
    Frame {
        msec: 1392
        hash: "b3f4c5cd728eaf2b791612a7fea64e7b"
    }
    Frame {
        msec: 1408
        hash: "3d01abd0b8a5a62d58a4c09546f212d8"
    }
    Frame {
        msec: 1424
        hash: "e76796498cf595c60d4b60cc0e320601"
    }
    Frame {
        msec: 1440
        hash: "1b31e96f2823e78a0c4029e7bc45b9f2"
    }
    Frame {
        msec: 1456
        hash: "f75c182dc24f4fabe1034ee494dba2ad"
    }
    Frame {
        msec: 1472
        hash: "646c12edadf350405709860381cfced6"
    }
    Frame {
        msec: 1488
        hash: "b6719406da9f2484fe55e3c69184f86c"
    }
    Frame {
        msec: 1504
        hash: "5456857d6d48d064df1cb3f35d8447b5"
    }
    Frame {
        msec: 1520
        hash: "8d1809b568345e1532fb6d9428fc9729"
    }
    Frame {
        msec: 1536
        hash: "5cffa76fe09a771a9f62a9f0392f0431"
    }
    Frame {
        msec: 1552
        hash: "8de59915e874ce829c691a19ac930f28"
    }
    Frame {
        msec: 1568
        hash: "9027bbf8121f70d26530f70423ec05b7"
    }
    Frame {
        msec: 1584
        hash: "d3d1d8b9f7b4eb74a8b7ae5cf19a8e20"
    }
    Frame {
        msec: 1600
        hash: "81ffcc0147e3124a3015deb7c0dbfd90"
    }
    Frame {
        msec: 1616
        hash: "ca0c96e908f05c4ee1af1f80d7b432aa"
    }
    Frame {
        msec: 1632
        hash: "2bdb6fbf942623856a6963c335794dd2"
    }
    Frame {
        msec: 1648
        hash: "18ac264d9ea9b592b0738f1cf732f678"
    }
    Frame {
        msec: 1664
        hash: "1ee9adbbae7b97dc050c82b8ed7b0aad"
    }
    Frame {
        msec: 1680
        hash: "b502390c452883ade550d2761bb09d3d"
    }
    Frame {
        msec: 1696
        hash: "31a6f573fbb3f545ee051e2290938004"
    }
    Frame {
        msec: 1712
        hash: "3be9788228d9e540313e75671319c5b7"
    }
    Frame {
        msec: 1728
        hash: "23cbd718154f939d8270674e8f7607f0"
    }
    Frame {
        msec: 1744
        hash: "5f7f49b894b80ddd7cdc544a49ec24a2"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 275; y: 170
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 278; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1760
        hash: "2a1ddee3d3a0c2a4fffab3988e35e274"
    }
    Frame {
        msec: 1776
        hash: "2a1ddee3d3a0c2a4fffab3988e35e274"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 279; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 282; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1792
        hash: "5594b9139480ba1c814509a049f9b6c5"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 290; y: 172
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1808
        hash: "d8729deb404f5b821264743943adb288"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 292; y: 172
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1824
        hash: "6de642baf7698ec65d48ccf0a1e8e7db"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 294; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 297; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1840
        hash: "f6732999861d1f638484a5aaa9cf0550"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 302; y: 170
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1856
        hash: "7cd7c1679838f35556bd4ee4565b7a86"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 305; y: 170
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 308; y: 169
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1872
        hash: "4276a4d9350503603b0c9c98552697b3"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 313; y: 169
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1888
        hash: "954a47627aee0a1128a78191bf32d984"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 331; y: 165
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1904
        hash: "360a47795f7f9389f82f2f55fa1fe83f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 340; y: 164
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1920
        hash: "19d4284791d0031342ba995bd17a7833"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 354; y: 163
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1936
        image: "test-pathview-2.2.png"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 361; y: 161
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 367; y: 160
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1952
        hash: "e9cd8fb810ecf39a90af039ead97aaf1"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 371; y: 160
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 376; y: 158
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1968
        hash: "42df1a0fbbe7cce5f2359d9e02696299"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 383; y: 157
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1984
        hash: "cc71434d6bd162386b80cb3b7e387116"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 387; y: 157
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 391; y: 157
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2000
        hash: "a130b471b3903f3f1d77f2306da2b92e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 394; y: 156
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2016
        hash: "5bdb7472e325651e891c115953afdb39"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 395; y: 156
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2032
        hash: "ab3a64b41c67a0b8a6c0830c0e0cb797"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 396; y: 156
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2048
        hash: "8eb1f2c8c02c2acf4262e05000045649"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 398; y: 156
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2064
        hash: "514220d357c4a26e4aaf9ed20d3f4f33"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 401; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2080
        hash: "e44526ef273048028d5989fc662eb7e6"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 403; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 406; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2096
        hash: "29ac091428a89cfcb4c52c08e0e10327"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 408; y: 154
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 409; y: 154
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2112
        hash: "82beb845af88fc9432dc104ff805a146"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 411; y: 153
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2128
        hash: "371392f267b2c1f4e29963506180e246"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 413; y: 153
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2144
        hash: "1da06d036cc0a2d2de34eee37b6981c0"
    }
    Frame {
        msec: 2160
        hash: "1da06d036cc0a2d2de34eee37b6981c0"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 414; y: 153
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2176
        hash: "4980de22342d1085e205401090777d24"
    }
    Frame {
        msec: 2192
        hash: "4980de22342d1085e205401090777d24"
    }
    Frame {
        msec: 2208
        hash: "4980de22342d1085e205401090777d24"
    }
    Frame {
        msec: 2224
        hash: "4980de22342d1085e205401090777d24"
    }
    Frame {
        msec: 2240
        hash: "4980de22342d1085e205401090777d24"
    }
    Frame {
        msec: 2256
        hash: "4980de22342d1085e205401090777d24"
    }
    Frame {
        msec: 2272
        hash: "4980de22342d1085e205401090777d24"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 412; y: 153
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2288
        hash: "e0a52543b976dc998615704c63b1f3e9"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 409; y: 154
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2304
        hash: "82beb845af88fc9432dc104ff805a146"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 401; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2320
        hash: "e44526ef273048028d5989fc662eb7e6"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 399; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 396; y: 156
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2336
        hash: "8eb1f2c8c02c2acf4262e05000045649"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 393; y: 158
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2352
        hash: "442958c3a705745204db96ff9902b7fc"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 392; y: 158
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2368
        hash: "a130b471b3903f3f1d77f2306da2b92e"
    }
    Frame {
        msec: 2384
        hash: "a130b471b3903f3f1d77f2306da2b92e"
    }
    Frame {
        msec: 2400
        hash: "a130b471b3903f3f1d77f2306da2b92e"
    }
    Frame {
        msec: 2416
        hash: "a130b471b3903f3f1d77f2306da2b92e"
    }
    Frame {
        msec: 2432
        hash: "a130b471b3903f3f1d77f2306da2b92e"
    }
    Frame {
        msec: 2448
        hash: "a130b471b3903f3f1d77f2306da2b92e"
    }
    Frame {
        msec: 2464
        hash: "a130b471b3903f3f1d77f2306da2b92e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 391; y: 159
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 390; y: 159
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2480
        hash: "374dc7c3ea0c93ac93a857a4620bc031"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 377; y: 159
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2496
        hash: "0b943f48b39053bfc906a4a47a37d68a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 331; y: 156
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2512
        hash: "099fbdf1560dd79b700914863406c904"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 294; y: 154
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 249; y: 151
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2528
        hash: "3aa1614cc49504d19e979ebf190f2970"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 129; y: 141
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2544
        hash: "837420c71a5010f25cccd05e5e9b3eec"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 9; y: 133
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2560
        hash: "871349fc09f418717231b8f8e20a7fff"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: -48; y: 128
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: -99; y: 126
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2576
        hash: "9b6022024aae22ec1f522fd00ed29e9b"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: -173; y: 129
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: -173; y: 129
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2592
        hash: "8d9410909ae259388fa94b3a60342608"
    }
    Frame {
        msec: 2608
        hash: "0ceb355351ac99458ba75776c11b3039"
    }
    Frame {
        msec: 2624
        hash: "61ca917ecc8ad4c35b7f2a3b828542bf"
    }
    Frame {
        msec: 2640
        hash: "fd5db933d1d8684b15eb5239d19d8919"
    }
    Frame {
        msec: 2656
        hash: "13f466a82ee22cabf5cbd2463f55b46a"
    }
    Frame {
        msec: 2672
        hash: "3b7f7880f5b568a0e45cd0e268822f3a"
    }
    Frame {
        msec: 2688
        hash: "cca22501c3b5a2ed4264ba060eeb1a6e"
    }
    Frame {
        msec: 2704
        hash: "efe5258ac5962d1d2bfa4286c1621830"
    }
    Frame {
        msec: 2720
        hash: "141998cff765a4e90836b871f229a1ca"
    }
    Frame {
        msec: 2736
        hash: "9d684675fa883d5488194effcb1d8d0a"
    }
    Frame {
        msec: 2752
        hash: "fa87f781048f264ddf447441a714ee50"
    }
    Frame {
        msec: 2768
        hash: "61b4992b9c52222345c9ada3148d50f9"
    }
    Frame {
        msec: 2784
        hash: "3e255a634d215746cb95f5d765335ea2"
    }
    Frame {
        msec: 2800
        hash: "d64a755e47a502244e7f14f2091f0ca6"
    }
    Frame {
        msec: 2816
        hash: "582562992b0652f995b439897182e0f8"
    }
    Frame {
        msec: 2832
        hash: "2d69b1a274c262faf5ce9ed3191c7d22"
    }
    Frame {
        msec: 2848
        hash: "36c04a2bd58124877a332bb6a262a7e5"
    }
    Frame {
        msec: 2864
        hash: "798711925da8f5034039dad86cc1fad1"
    }
    Frame {
        msec: 2880
        hash: "31495157a10c3bb4dd70cfd857fd07e6"
    }
    Frame {
        msec: 2896
        image: "test-pathview-2.3.png"
    }
    Frame {
        msec: 2912
        hash: "b81330eb50dbd39f1abcdb8ff1553d08"
    }
    Frame {
        msec: 2928
        hash: "ececcb86b76e9cd2f57585bd87e16bef"
    }
    Frame {
        msec: 2944
        hash: "2c37e2c24cf22a334cfcc6f2691ad9fb"
    }
    Frame {
        msec: 2960
        hash: "ad0572020d273dbca046357aa0f8bf3b"
    }
    Frame {
        msec: 2976
        hash: "51a469e059a5e1a3675db731f55209d3"
    }
    Frame {
        msec: 2992
        hash: "dca7d50a3faab1f049bece34bd16b8c4"
    }
    Frame {
        msec: 3008
        hash: "86dc86bafb01fa086caa3b22f9d393d9"
    }
    Frame {
        msec: 3024
        hash: "05754bd86070a6f01bf90ca2b964f695"
    }
    Frame {
        msec: 3040
        hash: "911ec290ba303f0cac258cbb893bbf78"
    }
    Frame {
        msec: 3056
        hash: "f27f29249426f46b8fb508372bcbb32d"
    }
    Frame {
        msec: 3072
        hash: "2f452e2d519f33ee03db67ebd7f69e3b"
    }
    Frame {
        msec: 3088
        hash: "35cf7747a75ea3f727c2fe1dae6136c5"
    }
    Frame {
        msec: 3104
        hash: "6773187693f52a8f2c0e358e379b4d21"
    }
    Frame {
        msec: 3120
        hash: "abca1f00f7ec60c8c80ba5345898e54b"
    }
    Frame {
        msec: 3136
        hash: "9bee1da64534da97de349e1ee973cc9c"
    }
    Frame {
        msec: 3152
        hash: "087df06ca720918482f2e29653c7fbac"
    }
    Frame {
        msec: 3168
        hash: "5b08911bf0975bd6615bf29294e4b1f5"
    }
    Frame {
        msec: 3184
        hash: "dead4bb3768b65418f68bae7dd0bf004"
    }
    Frame {
        msec: 3200
        hash: "6bfe4c866936d8ae509650419ae12455"
    }
    Frame {
        msec: 3216
        hash: "7428bdd9609a2594be08fdeac6ff1e17"
    }
    Frame {
        msec: 3232
        hash: "d02f9f693e0ae8c7034bf727064ec28a"
    }
    Frame {
        msec: 3248
        hash: "b6284efd849547bbfefc22ec77d61062"
    }
    Frame {
        msec: 3264
        hash: "4b78b647be8e918e85edab0c23b6f882"
    }
    Frame {
        msec: 3280
        hash: "c4a02c18ce3574d057e6a54b30efadb3"
    }
    Frame {
        msec: 3296
        hash: "d1d190010239d0b02a697d1c63c748ab"
    }
    Frame {
        msec: 3312
        hash: "b198689d11aa59d937297e6fcf675c93"
    }
    Frame {
        msec: 3328
        hash: "218f3371beea895aefd28aa874012dcc"
    }
    Frame {
        msec: 3344
        hash: "1135de1b9a4ebf1d2829546d3c3f3903"
    }
    Frame {
        msec: 3360
        hash: "773a64cc7bb8e99a25078f348986e28f"
    }
    Frame {
        msec: 3376
        hash: "e8ce58aeb18b3f56ebd3d6f61ac94657"
    }
    Frame {
        msec: 3392
        hash: "6de92679c32c7f3e9d9b6ba3a47e65eb"
    }
    Frame {
        msec: 3408
        hash: "339b37207af10ad986269e21ab37ff6d"
    }
    Frame {
        msec: 3424
        hash: "ac01f0708800fdfdacec67ac9e80602f"
    }
    Frame {
        msec: 3440
        hash: "9de89a748b1e18eb6ed94875af6f26de"
    }
    Frame {
        msec: 3456
        hash: "d091e4a93c2beafb0ce4b6dff6d5b05f"
    }
    Frame {
        msec: 3472
        hash: "9532271085864d2fde3aa6e572599588"
    }
    Frame {
        msec: 3488
        hash: "d00804b42ab1c1f082a9f394ff4d666e"
    }
    Frame {
        msec: 3504
        hash: "2c745f007353e6f8a7195470ba9492c2"
    }
    Frame {
        msec: 3520
        hash: "b4e952acb734ab1a608297fcb44fbe46"
    }
    Frame {
        msec: 3536
        hash: "75ceed3c2ddd557866145393fa50a12f"
    }
    Frame {
        msec: 3552
        hash: "8b83b80554dd4a1266184092d380554c"
    }
    Frame {
        msec: 3568
        hash: "973bddb1b2f9dbadd40c0de3ca7c3510"
    }
    Frame {
        msec: 3584
        hash: "5691b5bf54b50d4ff0a717873e001c00"
    }
    Frame {
        msec: 3600
        hash: "8b26b0aa8b06da031354c59d7fb41bf0"
    }
    Frame {
        msec: 3616
        hash: "45786c39a10b8e1cf399df98f3fb7ffb"
    }
    Frame {
        msec: 3632
        hash: "c6d0be03e167c16566372cc992604dfb"
    }
    Frame {
        msec: 3648
        hash: "8d6e057550632d143faf996a62bbd1cd"
    }
    Frame {
        msec: 3664
        hash: "7e3a321b95d5f62f0da2b10324b485b6"
    }
    Frame {
        msec: 3680
        hash: "e842f18dfd36947b2fa086a4d0bb2ec5"
    }
    Frame {
        msec: 3696
        hash: "a9359e143dae4113437a43cc00493479"
    }
    Frame {
        msec: 3712
        hash: "2eca61c837cca9beb6d1834eafe8c538"
    }
    Frame {
        msec: 3728
        hash: "bf2de49dc940043a955a075dcda1b52b"
    }
    Frame {
        msec: 3744
        hash: "bf2de49dc940043a955a075dcda1b52b"
    }
    Frame {
        msec: 3760
        hash: "bf2de49dc940043a955a075dcda1b52b"
    }
    Frame {
        msec: 3776
        hash: "bf2de49dc940043a955a075dcda1b52b"
    }
    Frame {
        msec: 3792
        hash: "bf2de49dc940043a955a075dcda1b52b"
    }
    Frame {
        msec: 3808
        hash: "bf2de49dc940043a955a075dcda1b52b"
    }
    Frame {
        msec: 3824
        hash: "bf2de49dc940043a955a075dcda1b52b"
    }
    Frame {
        msec: 3840
        hash: "bf2de49dc940043a955a075dcda1b52b"
    }
    Frame {
        msec: 3856
        image: "test-pathview-2.4.png"
    }
    Frame {
        msec: 3872
        hash: "bf2de49dc940043a955a075dcda1b52b"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 363; y: 156
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3888
        hash: "cf72e9ae81dcf833f7a48ffa348b8966"
    }
    Frame {
        msec: 3904
        hash: "cf72e9ae81dcf833f7a48ffa348b8966"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 363; y: 157
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3920
        hash: "cf72e9ae81dcf833f7a48ffa348b8966"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 361; y: 158
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 358; y: 158
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3936
        hash: "1cea11ee435caa8515797ee5c4fb79cb"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 352; y: 159
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3952
        hash: "0da1743b066a73dd19aff6b60ef76830"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 349; y: 160
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 342; y: 162
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3968
        hash: "ddace1df123421675bc9153c4017cdd0"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 327; y: 166
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3984
        hash: "0c57fe8eef4e41e326dbc82f7b6ae87b"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 320; y: 168
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 320; y: 168
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4000
        hash: "53968b4b57c09fe0b47e720031c1eed7"
    }
    Frame {
        msec: 4016
        hash: "2ab593b498892bf8bacef875e524284f"
    }
    Frame {
        msec: 4032
        hash: "da77708f525ab9d1d3f760595a1f9efa"
    }
    Frame {
        msec: 4048
        hash: "ce73ecb012139dda8e21cb0dce95582a"
    }
    Frame {
        msec: 4064
        hash: "086754b023addbbecf3b361382133279"
    }
    Frame {
        msec: 4080
        hash: "adcb9881f246993ff35af24f8750ea2f"
    }
    Frame {
        msec: 4096
        hash: "974b423c99316c9a5b2e097bb3a42fcc"
    }
    Frame {
        msec: 4112
        hash: "e37263abe79b203cfc4306aa7e5c4853"
    }
    Frame {
        msec: 4128
        hash: "0136eaf2704a5af80f8ba26bbb7f51da"
    }
    Frame {
        msec: 4144
        hash: "55fe0338e24aa91790f2cd466464acae"
    }
    Frame {
        msec: 4160
        hash: "9fa5eaebd34e2af136a2894f360301a5"
    }
    Frame {
        msec: 4176
        hash: "c48822e620b788947d8a5ec850d6313b"
    }
    Frame {
        msec: 4192
        hash: "ec763070f81e115a5e471923aa539683"
    }
    Frame {
        msec: 4208
        hash: "2aa84ad9ef88313a4c63e91bba959920"
    }
    Frame {
        msec: 4224
        hash: "14cf7ba825d704c4acc72670fd868d6c"
    }
    Frame {
        msec: 4240
        hash: "987bf945cd9c1cfe5bbb17442daa4f26"
    }
    Frame {
        msec: 4256
        hash: "5d4d80565bf4f522c79044d0df55a1fd"
    }
    Frame {
        msec: 4272
        hash: "d0a5ec7ff2c5b64c6691888412d0cc6d"
    }
    Frame {
        msec: 4288
        hash: "93750528b6f27df22423eb957a07b55f"
    }
    Frame {
        msec: 4304
        hash: "65fd0474f918bac61b46fde8ed8e3b59"
    }
    Frame {
        msec: 4320
        hash: "cd15f6499863ef84f0ad3b2ff48d6406"
    }
    Frame {
        msec: 4336
        hash: "65101124208b062de9718b34fb43425b"
    }
    Frame {
        msec: 4352
        hash: "cb42d683dc5e4020891601afb0a77947"
    }
    Frame {
        msec: 4368
        hash: "88fdddbf2f766ffff7e77c7612d9cfee"
    }
    Frame {
        msec: 4384
        hash: "776c63f1bbc40624d7fedd6141fbdd97"
    }
    Frame {
        msec: 4400
        hash: "24f11b5abb33d8f180a56fca6f15ef45"
    }
    Frame {
        msec: 4416
        hash: "71d9ab083d15b57336ee278793815713"
    }
    Frame {
        msec: 4432
        hash: "b48e64cb1b8b39e7001af4e7c7d22098"
    }
    Frame {
        msec: 4448
        hash: "587ef2440cd021038cc902a3b1839ff4"
    }
    Frame {
        msec: 4464
        hash: "99e0485247c907c5b6e0f8d5dc7b8977"
    }
    Frame {
        msec: 4480
        hash: "3b2496d61eefaa413f0688afed150749"
    }
    Frame {
        msec: 4496
        hash: "0144d27095182c58e50ae1ccdbfaa05e"
    }
    Frame {
        msec: 4512
        hash: "06237be375826d2434dc564dd2eaf165"
    }
    Frame {
        msec: 4528
        hash: "7235d512503b134ac267b7128163eea2"
    }
    Frame {
        msec: 4544
        hash: "5d5f7ff9bd0a4aa316b764bec8524fe0"
    }
    Frame {
        msec: 4560
        hash: "9be01e649140f950cd882af2e8e1e27c"
    }
    Frame {
        msec: 4576
        hash: "0773e5d219d6fc4f2d385fd1bcd17f93"
    }
    Frame {
        msec: 4592
        hash: "32979d6f14c1aeca1f7ac0c5a330bbdc"
    }
    Frame {
        msec: 4608
        hash: "6f87571a59aa358755d80e94894fe7a9"
    }
    Frame {
        msec: 4624
        hash: "0e31c55386e8838f52024c49d4929710"
    }
    Frame {
        msec: 4640
        hash: "7d6c89f5fae7990643687512f2294449"
    }
    Frame {
        msec: 4656
        hash: "f8542ff33dbad93ed51a0801bd8af778"
    }
    Frame {
        msec: 4672
        hash: "8bed907fe5b04eec118ac4e7759386ae"
    }
    Frame {
        msec: 4688
        hash: "d84facac927215d8d83bd9e375fbace1"
    }
    Frame {
        msec: 4704
        hash: "f1c8b7dc9897713487fcc62c697f41ff"
    }
    Frame {
        msec: 4720
        hash: "611c45384b2abd883a4e3ec3bb30ebd3"
    }
    Frame {
        msec: 4736
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4752
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4768
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4784
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4800
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4816
        image: "test-pathview-2.5.png"
    }
    Frame {
        msec: 4832
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4848
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4864
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4880
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4896
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4912
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4928
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4944
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4960
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4976
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 4992
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 5008
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 5024
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 5040
        hash: "63e075c2cac3770e657217989cc7d80f"
    }
    Frame {
        msec: 5056
        hash: "34967fb7248c860643bdc01e0135309f"
    }
}
