import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "test-pathview.0.png"
    }
    Frame {
        msec: 32
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 48
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 64
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 80
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 96
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 112
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 128
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 144
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 160
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 176
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 192
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 208
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 224
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 240
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 256
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 272
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 288
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 304
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 320
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 336
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 352
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 368
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 384
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 400
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 416
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 432
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 448
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 464
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 480
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 496
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 512
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 528
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Frame {
        msec: 544
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 363; y: 161
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 560
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 362; y: 160
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 576
        hash: "b9fed927475786f6f7aefc554cfc1afe"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 361; y: 159
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 357; y: 159
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 592
        hash: "731c8547a72c64ac86aec87c0a9a12cb"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 348; y: 157
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 330; y: 157
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 608
        hash: "d9d7dd7ea05499f028964fdd11af0fe6"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 286; y: 161
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 624
        hash: "361879f350c448a484b71a9e7a42b87f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 254; y: 163
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 640
        hash: "998da4b3e36ee3e17deb2b5a097661da"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 240; y: 165
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 225; y: 167
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 656
        hash: "1b3f9758bd9842cc9545b494499f87c4"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 191; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 672
        hash: "7e87f7c233dad50549e4bdafe10bb48e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 174; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 153; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 153; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 688
        hash: "01ceb2fea81f2192ab11d7d6e1df879a"
    }
    Frame {
        msec: 704
        hash: "9afa862248bd527e07374a5c2f2036a1"
    }
    Frame {
        msec: 720
        hash: "e06439495148bfbf059cfe2b5df22840"
    }
    Frame {
        msec: 736
        hash: "b206a28d6f3be8cba9595849328b27b8"
    }
    Frame {
        msec: 752
        hash: "646e4529bf554dceee0140ec56a02d1c"
    }
    Frame {
        msec: 768
        hash: "31bdcf1f178d65e033e23dfbdcb9dc5f"
    }
    Frame {
        msec: 784
        hash: "b4e897356814ca2dddbc3644b1782f36"
    }
    Frame {
        msec: 800
        hash: "669e5d682aae8727640e0e0f4e855a60"
    }
    Frame {
        msec: 816
        hash: "892007b1a379c617412502499df92d01"
    }
    Frame {
        msec: 832
        hash: "f4d66daa2d428aa712a73ded2de7a361"
    }
    Frame {
        msec: 848
        hash: "0c21e69bed6dc2d6b7c23c20714aca67"
    }
    Frame {
        msec: 864
        hash: "189909bdbfeb1f02ad527fbc438d567d"
    }
    Frame {
        msec: 880
        hash: "b2fcbc0657474e1b6d27e1f2f93be35b"
    }
    Frame {
        msec: 896
        hash: "4407d7ad1b6a40b2355145aee136ff15"
    }
    Frame {
        msec: 912
        hash: "347ada687af0a97f0a862a1f3a1132be"
    }
    Frame {
        msec: 928
        hash: "db6217ff0194c5a3f9ca9ea7e3b3dfd8"
    }
    Frame {
        msec: 944
        hash: "8a94ca0ee93daaa1bdcdbfc8a80713c1"
    }
    Frame {
        msec: 960
        hash: "ab24d0c8545518cbaff876976247be2c"
    }
    Frame {
        msec: 976
        image: "test-pathview.1.png"
    }
    Frame {
        msec: 992
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1008
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1024
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1040
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1056
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1072
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1088
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1104
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1120
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1136
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1152
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Frame {
        msec: 1168
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 378; y: 161
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1184
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 376; y: 161
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1200
        hash: "1535dea92038cf87395a616841fd9bf6"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 374; y: 161
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 373; y: 161
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1216
        hash: "c612bb9906f18786ef7cc6f4e56de218"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 367; y: 160
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 353; y: 160
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1232
        hash: "ffec210dd863ed32a780506f61b06056"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 328; y: 157
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 303; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1248
        hash: "9613c658f267d19b84d6e7ef2a676fed"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 280; y: 153
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 253; y: 151
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1264
        hash: "8c5dd8d0f9f434530b20e14a84af9f46"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 230; y: 151
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 211; y: 151
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1280
        hash: "a956e8e9ca8958c387f8f5ce374cdec9"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 193; y: 153
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 193; y: 153
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1296
        hash: "712e865d894f179cfd9d86b08e60811a"
    }
    Frame {
        msec: 1312
        hash: "db5c1f2af2e72ff4edce83cb342b5263"
    }
    Frame {
        msec: 1328
        hash: "834f0aa26c66234491468c1b27a2d329"
    }
    Frame {
        msec: 1344
        hash: "78a2a4b60db730a7367bc77e1dfc1a1b"
    }
    Frame {
        msec: 1360
        hash: "a8ff2277b5f7d515bc5a9af1f0e77197"
    }
    Frame {
        msec: 1376
        hash: "e05d730624025000b831860f5b99e8ac"
    }
    Frame {
        msec: 1392
        hash: "54aa124492ea742e4327f1d2b45ab620"
    }
    Frame {
        msec: 1408
        hash: "bc700bee41ac384a2555723b010e9041"
    }
    Frame {
        msec: 1424
        hash: "26f66098c505cea4715a89b6a2232759"
    }
    Frame {
        msec: 1440
        hash: "00f3255a3ead315410d8c0d338779689"
    }
    Frame {
        msec: 1456
        hash: "154e7d86d7602ebba38a0d63b211894d"
    }
    Frame {
        msec: 1472
        hash: "87cf2bff69ebd75af69d0a7c7f668b07"
    }
    Frame {
        msec: 1488
        hash: "f221b870ecccb1669b6223e5431c31d1"
    }
    Frame {
        msec: 1504
        hash: "40a9d4c522d9fd831be2ca698ac10670"
    }
    Frame {
        msec: 1520
        hash: "7ad47479d99fd4d9fde96fef242bdc20"
    }
    Frame {
        msec: 1536
        hash: "b91912801c790d849399306c693a4d33"
    }
    Frame {
        msec: 1552
        hash: "e5c8d361abcbc15df0b0b82728cb5b84"
    }
    Frame {
        msec: 1568
        hash: "3f2f82c925e93d4593581cdba16f361f"
    }
    Frame {
        msec: 1584
        hash: "7007fd0595c188a9a5b3ff31b0514aa5"
    }
    Frame {
        msec: 1600
        hash: "118661091df765ae35c152c7fe818029"
    }
    Frame {
        msec: 1616
        hash: "0a8edd2a35f7921ced6e3aa7e571bc4b"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 339; y: 152
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 334; y: 152
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1632
        hash: "ef734ce4d7e1aee19a78b743c9923f90"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 245; y: 152
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 161; y: 148
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1648
        hash: "09a9925d5ec2fd03cfbf469bc22bf201"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 139; y: 150
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 139; y: 150
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1664
        hash: "6babcbf5582d5ed8f0cf52e233867055"
    }
    Frame {
        msec: 1680
        hash: "94dae9d52f3523e17f3f0e59ca24a069"
    }
    Frame {
        msec: 1696
        hash: "0d417d25893a0454a729f5c23a2a6c28"
    }
    Frame {
        msec: 1712
        hash: "afd1bbca1dcfea8d1f0a340d86b07fa8"
    }
    Frame {
        msec: 1728
        hash: "97e98982742b94dba8b6cb59397bcb66"
    }
    Frame {
        msec: 1744
        hash: "a0ad8cbbd0daa0afd3831e8a071b9a0e"
    }
    Frame {
        msec: 1760
        hash: "f71826bcd6ea91d2f64d627a390c379d"
    }
    Frame {
        msec: 1776
        hash: "7699da01cf1ee9a7f404ab053241b530"
    }
    Frame {
        msec: 1792
        hash: "6aba727ecc562d7b5555eae427e6978b"
    }
    Frame {
        msec: 1808
        hash: "ef9c6daa5b04b0be9159594e04524fba"
    }
    Frame {
        msec: 1824
        hash: "6293ede5de83f3b01a3b4d8d87648089"
    }
    Frame {
        msec: 1840
        hash: "c3b34d8592f88622cad0f9353d08e739"
    }
    Frame {
        msec: 1856
        hash: "880f3cb9d5dbe06cdf17e3a953d4562d"
    }
    Frame {
        msec: 1872
        hash: "ed381ce920863a5a6627f383a88ea2fe"
    }
    Frame {
        msec: 1888
        hash: "b5bc40b8c4abb6458aeb67eda73507b6"
    }
    Frame {
        msec: 1904
        hash: "482cb61b7fac4b1654483f846b8b6717"
    }
    Frame {
        msec: 1920
        hash: "e1a4a16d2cf5132a9fbb0869ed6082d9"
    }
    Frame {
        msec: 1936
        image: "test-pathview.2.png"
    }
    Frame {
        msec: 1952
        hash: "f8874aaab1e65cf9b86d6b5174c3d2c8"
    }
    Frame {
        msec: 1968
        hash: "d8490adeaa793352b812e832f4cb079a"
    }
    Frame {
        msec: 1984
        hash: "85fdb99926ba34a25fa964df11af9a5a"
    }
    Frame {
        msec: 2000
        hash: "ad137a75981c181838d97cbe313063ac"
    }
    Frame {
        msec: 2016
        hash: "bfa5cecfc0058b56ca66aa816ea098dc"
    }
    Frame {
        msec: 2032
        hash: "53fe3960c2f332eb099fedd8421fcc94"
    }
    Frame {
        msec: 2048
        hash: "61b99ff526560c1589d2fc8737af2af2"
    }
    Frame {
        msec: 2064
        hash: "f9dd63709bed985f5d691d27c0d32484"
    }
    Frame {
        msec: 2080
        hash: "964c20ada9ad9e83edd9b429bf681b83"
    }
    Frame {
        msec: 2096
        hash: "997bc44a319c8ce8212387f7564c4005"
    }
    Frame {
        msec: 2112
        hash: "892eda6e7446321483ffb1dbf44a0432"
    }
    Frame {
        msec: 2128
        hash: "62068dca6da7227882b6c3bc147c6f24"
    }
    Frame {
        msec: 2144
        hash: "2cd0c351c53234d4bbf4d2c74d313f59"
    }
    Frame {
        msec: 2160
        hash: "cf812f971bb4f8ab3116cf2b14c325df"
    }
    Frame {
        msec: 2176
        hash: "be296bd9ab4c38d95e6d7d445d8c7f68"
    }
    Frame {
        msec: 2192
        hash: "536d0214c8c3f69ce8d4e1585128b2b8"
    }
    Frame {
        msec: 2208
        hash: "f71452a0a6ef80758800d67e601a162b"
    }
    Frame {
        msec: 2224
        hash: "e57c099beb70d0a4ca2cbc94a2c3887e"
    }
    Frame {
        msec: 2240
        hash: "84cea22f64ff8b8838a7db0b19af1a4e"
    }
    Frame {
        msec: 2256
        hash: "04aa0d5d089779977f569d0f849b97dd"
    }
    Frame {
        msec: 2272
        hash: "85b52e125142d52d531132939930dd93"
    }
    Frame {
        msec: 2288
        hash: "19bc7b318c21a6ce2be8ebde2e624fc3"
    }
    Frame {
        msec: 2304
        hash: "9cc744249cb031f0400e87893c1642af"
    }
    Frame {
        msec: 2320
        hash: "a834706bbf573f37cf9f59c6c6cbbfa5"
    }
    Frame {
        msec: 2336
        hash: "8db3eea9d47a162d8b0ee9cd18e194f3"
    }
    Frame {
        msec: 2352
        hash: "29da9b8da8f572ace93250abb8626a90"
    }
    Frame {
        msec: 2368
        hash: "179b74316d885f9ee41066b9c475b57f"
    }
    Frame {
        msec: 2384
        hash: "35464509ef5a9919af46a30d40c3edc7"
    }
    Frame {
        msec: 2400
        hash: "aadec42355d38d149421ef6c93783e69"
    }
    Frame {
        msec: 2416
        hash: "cb8609791270e8e3c13da4579f85595f"
    }
    Frame {
        msec: 2432
        hash: "93e81e036a1bc30cc63ce703f8f43a34"
    }
    Frame {
        msec: 2448
        hash: "d08d18adf9ca92cd6597c2f51ae90383"
    }
    Frame {
        msec: 2464
        hash: "f54ec103787023647beaa4b992340385"
    }
    Frame {
        msec: 2480
        hash: "61c9f72d78fce0b966a278abacc97ce6"
    }
    Frame {
        msec: 2496
        hash: "5b0500ed0562b11280c3424412f74188"
    }
    Frame {
        msec: 2512
        hash: "b8ee7bc1e94ce35bf946ee71fa03d72c"
    }
    Frame {
        msec: 2528
        hash: "60ec6aceeaf82fc730c3df55b5c06f90"
    }
    Frame {
        msec: 2544
        hash: "01cc732bad8b28483e79115c117ee26d"
    }
    Frame {
        msec: 2560
        hash: "b39c8d373524ba679c8567d16e6c5fe0"
    }
    Frame {
        msec: 2576
        hash: "2474476dfd021ff485c3a127bd22367e"
    }
    Frame {
        msec: 2592
        hash: "1342a1a0f6bc02159de1be058cf2411b"
    }
    Frame {
        msec: 2608
        hash: "a9721b64b9a5526335937245302249ae"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 72; y: 121
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2624
        hash: "109dc503ee86e731f52d25908daf5d36"
    }
    Frame {
        msec: 2640
        hash: "94998dbab6792c518ca1f37f060f1d4b"
    }
    Frame {
        msec: 2656
        hash: "3146ba4e63fa74279939b8de935f067c"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 73; y: 121
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 74; y: 122
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2672
        hash: "1aaea4143076bf8ba8190d94fcc89e64"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 79; y: 123
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 95; y: 129
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2688
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 126; y: 138
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 161; y: 148
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2704
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 194; y: 158
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 239; y: 169
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2720
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 280; y: 178
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 313; y: 185
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2736
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 344; y: 191
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 344; y: 191
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2752
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2768
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2784
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2800
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2816
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2832
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2848
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2864
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2880
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2896
        image: "test-pathview.3.png"
    }
    Frame {
        msec: 2912
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2928
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2944
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2960
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2976
        hash: "a0d8bb20189c3c65e5e72671788d9493"
    }
    Frame {
        msec: 2992
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3008
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3024
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3040
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3056
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3072
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3088
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 152; y: 143
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3104
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3120
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3136
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3152
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3168
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 151; y: 144
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3184
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Frame {
        msec: 3200
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 152; y: 145
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 153; y: 145
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3216
        hash: "1236a317e60f7ae3d3fb2fb521bad2a2"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 155; y: 146
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 157; y: 146
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3232
        hash: "1b604ea70459a768fb37a6333000174b"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 161; y: 147
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 164; y: 148
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3248
        hash: "25e0aabe364085a61b4572ef015dac2c"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 166; y: 148
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 168; y: 149
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3264
        hash: "ee6fc5c1de08e6f13f23b26829d2cba2"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 170; y: 150
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 171; y: 150
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3280
        hash: "b077c59359d047738d9ba739f591393b"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 175; y: 150
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 177; y: 151
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3296
        hash: "2cc0b8d7bd088f2277f5e939c234114c"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 180; y: 152
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 183; y: 152
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3312
        hash: "64703db84cd5bda3109546293783804d"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 187; y: 153
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 191; y: 154
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3328
        hash: "137cd88932ad1fdbfdbf1a80cccf7b3f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 193; y: 154
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 195; y: 154
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3344
        hash: "ff9011d861c64bcad214b52cb4245583"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 197; y: 154
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 199; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3360
        hash: "c3f0132e472d29ddee95c7349243d33e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 202; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 202; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3376
        hash: "42ae9c21dce6a7cd59de228dac775dd5"
    }
    Frame {
        msec: 3392
        hash: "3f8631caf6a98d83356b188d6f94e9a6"
    }
    Frame {
        msec: 3408
        hash: "b2788cd1939a6dd42f12d8fd1282a122"
    }
    Frame {
        msec: 3424
        hash: "0d1ab6e9f2780be0c392d20f4b3b9619"
    }
    Frame {
        msec: 3440
        hash: "03fdd91b352798b1ff958c23c0bc5f35"
    }
    Frame {
        msec: 3456
        hash: "028fee3630fdb3cf862213c0466a56fe"
    }
    Frame {
        msec: 3472
        hash: "3ab76009ca029723e5cf0bf9bc154102"
    }
    Frame {
        msec: 3488
        hash: "866c59b7dd545364b70ddbf21a8ee874"
    }
    Frame {
        msec: 3504
        hash: "9b4ff972b1055db38900fc0c5007e7b0"
    }
    Frame {
        msec: 3520
        hash: "cbe0073c84617e23f0679a08c1a78492"
    }
    Frame {
        msec: 3536
        hash: "374a5e6070dd628ed031e80d44be1f3f"
    }
    Frame {
        msec: 3552
        hash: "4d16c81f877585a82549cfc4f68c574d"
    }
    Frame {
        msec: 3568
        hash: "64b2b4c374a730b138b3573095f45d2c"
    }
    Frame {
        msec: 3584
        hash: "26c59f4131fdb01ac4771231341c75c3"
    }
    Frame {
        msec: 3600
        hash: "bf6a3fdb7c516ca9cfc09f1059cc8cdf"
    }
    Frame {
        msec: 3616
        hash: "1bfb86796087cd293c68205cce6ac294"
    }
    Frame {
        msec: 3632
        hash: "e0f76f8fc7bd7756a4e004655f97f782"
    }
    Frame {
        msec: 3648
        hash: "61d3aa5f827452482d8a4a903fe64acc"
    }
    Frame {
        msec: 3664
        hash: "c8e42d3a5df195eaa091e50fc9dcd51e"
    }
    Frame {
        msec: 3680
        hash: "bb684dccf4c0a74dc091fb78c1be4f2b"
    }
    Frame {
        msec: 3696
        hash: "54341e5a76fb4657021c41e6e3f3d496"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 117; y: 142
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 118; y: 142
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3712
        hash: "435ee710e108df42f659250ad7dbdb5e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 118; y: 143
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3728
        hash: "0c7078ec0d4a1dea84e0fba06323c533"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 119; y: 143
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 120; y: 143
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3744
        hash: "854103790c02ca86fa011ef1b0f2be0a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 121; y: 144
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 122; y: 144
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3760
        hash: "1a5995196e5bb4d1464ca76191af72d5"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 123; y: 144
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 124; y: 144
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3776
        hash: "397bbd080cae99790621642fab6ded91"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 126; y: 144
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 129; y: 145
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3792
        hash: "66ecad306911060329dcf7695c358e87"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 132; y: 145
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 135; y: 146
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3808
        hash: "c06da5f40f3f59f576a1d540d0b3244f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 139; y: 147
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 143; y: 149
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3824
        hash: "a88d97691539dce19af4c14baf610275"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 147; y: 150
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 152; y: 151
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3840
        hash: "a07dca2c0014609ca5241612550992f5"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 156; y: 152
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 161; y: 153
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3856
        image: "test-pathview.4.png"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 168; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 175; y: 155
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3872
        hash: "e5a4e76dd607ba1bae97aaf184ee009a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 184; y: 157
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 194; y: 158
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3888
        hash: "bb1d2614e590562479fc8d301bc7402f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 203; y: 160
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 211; y: 160
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3904
        hash: "5d9fd2238666d3ae04613f1bba0fab05"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 221; y: 162
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 231; y: 162
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3920
        hash: "b12a944cb5e593afbb21a10453879b52"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 241; y: 162
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 251; y: 164
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3936
        hash: "2f04c990978627b86fb2ad04579db0db"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 276; y: 167
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3952
        hash: "e7ddf142fc36174fcaaa70b9340ef7a8"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 288; y: 167
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 301; y: 169
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3968
        hash: "4fce53c6f5347fe03ecf17b07fabe3ac"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 312; y: 169
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 324; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 324; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3984
        hash: "75a0ec2c0158c55a90147c3f4afaa19c"
    }
    Frame {
        msec: 4000
        hash: "e89e98b7c1f36b74c664c77e121dedcb"
    }
    Frame {
        msec: 4016
        hash: "f4c1e52a7b97a25fba640be2a1430d2d"
    }
    Frame {
        msec: 4032
        hash: "be58ca8f63dac8373825231512f483ca"
    }
    Frame {
        msec: 4048
        hash: "755b16d4be00cb52595d42775d6227ac"
    }
    Frame {
        msec: 4064
        hash: "c62f1ebbb1e4ae4ca22c060078d6240b"
    }
    Frame {
        msec: 4080
        hash: "5f1187e9530584f9eb81ce1ce8267da0"
    }
    Frame {
        msec: 4096
        hash: "5dc9921e9ddf15ee0457fcdc834544c5"
    }
    Frame {
        msec: 4112
        hash: "efacedc2782435ef4e269e6956fb3547"
    }
    Frame {
        msec: 4128
        hash: "5b356dd3082f6b0920bb41d332595ce1"
    }
    Frame {
        msec: 4144
        hash: "5d8afcc1abd890beb2badf85bcf02897"
    }
    Frame {
        msec: 4160
        hash: "03c56ab4fea11cce19fcbb62dccb7683"
    }
    Frame {
        msec: 4176
        hash: "236254ce32a8e06dc42f2fd3c9ac6c7c"
    }
    Frame {
        msec: 4192
        hash: "4beb33da77bc2b41eb882a2a5cdeb539"
    }
    Frame {
        msec: 4208
        hash: "b345470adead1ffb3af4d1091ffbd95c"
    }
    Frame {
        msec: 4224
        hash: "c2677f1653b08952338a5c26a724ebe7"
    }
    Frame {
        msec: 4240
        hash: "45b6633acf0ac28c5b5462920cf61282"
    }
    Frame {
        msec: 4256
        hash: "26a9a6609ce8eee1f744c2bd43494f22"
    }
    Frame {
        msec: 4272
        hash: "9373a8010a05d05cb5b3c2ec75359493"
    }
    Frame {
        msec: 4288
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Frame {
        msec: 4304
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Frame {
        msec: 4320
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Frame {
        msec: 4336
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Frame {
        msec: 4352
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Frame {
        msec: 4368
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Frame {
        msec: 4384
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Frame {
        msec: 4400
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 112; y: 126
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4416
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 112; y: 128
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4432
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 114; y: 128
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 115; y: 130
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4448
        hash: "d0c561761825512a02a9e3640139cadc"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 116; y: 130
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 119; y: 132
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4464
        hash: "0e7554f077e2d6d8c6cf9496b20ab009"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 122; y: 134
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 131; y: 138
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4480
        hash: "d6e78f43c971abcc1d2aadb96e8b80b0"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 147; y: 144
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 168; y: 151
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4496
        hash: "10d8e0ee5bd432c639963c9cedd25b85"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 190; y: 157
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 219; y: 164
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4512
        hash: "53e142d6b0112644d75df29f7865fbb4"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 269; y: 171
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4528
        hash: "9609807e6c2a27a8b9f1d5c878c3dadf"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 319; y: 176
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4544
        hash: "a0a1e5fd37e9d8033f182f4f2b20fd26"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 361; y: 180
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4560
        hash: "b40e553dc373e4018488d5421b9a8914"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 406; y: 185
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4576
        hash: "22e36512a0af86fac12c09f735dcb1f7"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 428; y: 187
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 428; y: 187
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4592
        hash: "70e9ad0f56e4c37f8684e38f614b889d"
    }
    Frame {
        msec: 4608
        hash: "0754126f5738e3dcec35fc1ef65fdec3"
    }
    Frame {
        msec: 4624
        hash: "b3d84ceeecc294d21bc09a3197195c20"
    }
    Frame {
        msec: 4640
        hash: "ce00501e194b1056edf1ebd43b954a70"
    }
    Frame {
        msec: 4656
        hash: "793f41ac2568530e6d630446216833dc"
    }
    Frame {
        msec: 4672
        hash: "e8573de724b653439bde85c15e9555ab"
    }
    Frame {
        msec: 4688
        hash: "bfb3f3645c7b2425b686ac23bcef82b8"
    }
    Frame {
        msec: 4704
        hash: "faa78596e208c2cf4593ea25e31fabde"
    }
    Frame {
        msec: 4720
        hash: "f1b0931bffce37abfe5a6d635f1f8454"
    }
    Frame {
        msec: 4736
        hash: "0975630a55bfd56eb3e39426c1c3f1e5"
    }
    Frame {
        msec: 4752
        hash: "98f1d79153a8009123abc94141375779"
    }
    Frame {
        msec: 4768
        hash: "d864817f877a9eeb44c665518ea19687"
    }
    Frame {
        msec: 4784
        hash: "79745c267d14e7790e1bb3a7e76f20b4"
    }
    Frame {
        msec: 4800
        hash: "ec038d4cec64b847711fa221f808bead"
    }
    Frame {
        msec: 4816
        image: "test-pathview.5.png"
    }
    Frame {
        msec: 4832
        hash: "ef7b3f93abbf210f8f0d38a58380dc8f"
    }
    Frame {
        msec: 4848
        hash: "f0eea63127df25f7f818596fc034fef8"
    }
    Frame {
        msec: 4864
        hash: "8000dee3ea54522a8193a7f9f2e86023"
    }
    Frame {
        msec: 4880
        hash: "111485ebaf93aae4f5e0a83da898bbac"
    }
    Frame {
        msec: 4896
        hash: "4b2dee1fd88dcaeabc8235f6a0e5c090"
    }
    Frame {
        msec: 4912
        hash: "5e560c777d0294dfa8f249232bfcf3a2"
    }
    Frame {
        msec: 4928
        hash: "d8b490092ca5ce3ef9b078f4768c382a"
    }
    Frame {
        msec: 4944
        hash: "28b2bbc3fd19786dd9c0ab718141c525"
    }
    Frame {
        msec: 4960
        hash: "d1a61000ebc5a475c0223dde649c8054"
    }
    Frame {
        msec: 4976
        hash: "d3e8aae08a2518c039d6bda80fc520a4"
    }
    Frame {
        msec: 4992
        hash: "9f3bd8654adb9af0457dd50ff71fcd43"
    }
    Frame {
        msec: 5008
        hash: "befe00fef613b7616e2dc668a5ed59c7"
    }
    Frame {
        msec: 5024
        hash: "24e84e6998389aa119d7d9e0ac2206ac"
    }
    Frame {
        msec: 5040
        hash: "2d3d2b66bf016c8e499f527dbf8923db"
    }
    Frame {
        msec: 5056
        hash: "52d24673729dbd53d3227675b7001b24"
    }
    Frame {
        msec: 5072
        hash: "4e5c807682d7b6b7839c047a7fb4ad93"
    }
    Frame {
        msec: 5088
        hash: "319affea47c4a0b0e2c3db51b85430bc"
    }
    Frame {
        msec: 5104
        hash: "344962f0b88c7e8a33df71b4708fd1c0"
    }
    Frame {
        msec: 5120
        hash: "ac099ba8a5639b9c83b6f58f2b5bcf93"
    }
    Frame {
        msec: 5136
        hash: "2f8e57c93289dcdc758281531300e949"
    }
    Frame {
        msec: 5152
        hash: "e4cc3bdf6068064bcfdd0014cc301e65"
    }
    Frame {
        msec: 5168
        hash: "598c8a33e2bbf47b21df8b0636e0f0bc"
    }
    Frame {
        msec: 5184
        hash: "6aea67c85370eee8447a22e2b9e8c44c"
    }
    Frame {
        msec: 5200
        hash: "39e27a3376f4aba8510f7b0d90ca0e33"
    }
    Frame {
        msec: 5216
        hash: "0ff93a16a07af43bd5e22a2b00fd2588"
    }
    Frame {
        msec: 5232
        hash: "8b6004368b9b0a766f6b519820fe1ff6"
    }
    Frame {
        msec: 5248
        hash: "5d92c0a12ff138d1b2c75bd042be4ea2"
    }
    Frame {
        msec: 5264
        hash: "4386b0abe49106a0174154c726c301f6"
    }
    Frame {
        msec: 5280
        hash: "832da8d2a86caa3ca96f33d2cd49178e"
    }
    Frame {
        msec: 5296
        hash: "efee6ab1ba4a1112f2129aad12825667"
    }
    Frame {
        msec: 5312
        hash: "f20a7e67a4789c559b0b0a7656bd89b1"
    }
    Frame {
        msec: 5328
        hash: "350cc8c0085a8f79c9ea8880737a0b75"
    }
    Frame {
        msec: 5344
        hash: "b19715b4029ea489debf7c5a269aca98"
    }
    Frame {
        msec: 5360
        hash: "f383fcaf603af41650c5622bfaf136b3"
    }
    Frame {
        msec: 5376
        hash: "0c62a442367fc0bac5117da1327ed39a"
    }
    Frame {
        msec: 5392
        hash: "323ba45d158d983f359211f1a87b7ebd"
    }
    Frame {
        msec: 5408
        hash: "aeed1a31b8b77dac2c2858969ff2d86c"
    }
    Frame {
        msec: 5424
        hash: "27a9357730a97846ffeddd18492df04d"
    }
    Frame {
        msec: 5440
        hash: "42f78593e64585b33c8854e8ea92710e"
    }
    Frame {
        msec: 5456
        hash: "064f5cec99b9a351bebe2088019f46d1"
    }
    Frame {
        msec: 5472
        hash: "d3669826f94aa2afc1069ab967f677a3"
    }
    Frame {
        msec: 5488
        hash: "a118cf8892d29e6b70b4e65e42380c15"
    }
    Frame {
        msec: 5504
        hash: "f254260f01ff4697e9e3146cc106140d"
    }
    Frame {
        msec: 5520
        hash: "ec062b2bb87444115c2e8744b7f80bde"
    }
    Frame {
        msec: 5536
        hash: "4d45522a4e4253c810cac9cbf24c9b76"
    }
    Frame {
        msec: 5552
        hash: "532c3d3ead73836948a1036e8e69cadf"
    }
    Frame {
        msec: 5568
        hash: "4debea14aeac85ff4e64387938d8b010"
    }
    Frame {
        msec: 5584
        hash: "d8940cf6e39a1bd5e7216a83ce87a676"
    }
    Frame {
        msec: 5600
        hash: "fba6485f8a60a38ce2f3110137b1f2df"
    }
    Frame {
        msec: 5616
        hash: "8a8909b114332dd932b784a2640e9ff4"
    }
    Frame {
        msec: 5632
        hash: "fd901422400333c137240ef5f91928a3"
    }
    Frame {
        msec: 5648
        hash: "97b84a957515d5823e381fdd86d31fb8"
    }
    Frame {
        msec: 5664
        hash: "f3547ea694b88dd7d2fb8b04d6bf76a9"
    }
    Frame {
        msec: 5680
        hash: "9eb0da29d0c323b45e62d31bee97ce8c"
    }
    Frame {
        msec: 5696
        hash: "9d814096d27e9fbcffdf7e29866e0059"
    }
    Frame {
        msec: 5712
        hash: "6087185e1e8bf17545a7372be2990ab2"
    }
    Frame {
        msec: 5728
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5744
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5760
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5776
        image: "test-pathview.6.png"
    }
    Frame {
        msec: 5792
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5808
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5824
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5840
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5856
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5872
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5888
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5904
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5920
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5936
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5952
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5968
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 5984
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 6000
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 6016
        hash: "82e534c416dfe884e5abc2f91d902484"
    }
    Frame {
        msec: 6032
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6048
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6064
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6080
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6096
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6112
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6128
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6144
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6160
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6176
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6192
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6208
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6224
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6240
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6256
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
    Frame {
        msec: 6272
        hash: "6839b467f32eaa79d4c1ce4905145350"
    }
}
