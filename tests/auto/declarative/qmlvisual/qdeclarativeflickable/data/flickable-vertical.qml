import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "flickable-vertical.0.png"
    }
    Frame {
        msec: 32
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 48
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 64
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 80
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 96
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 112
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 128
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 144
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 160
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 176
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 192
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 208
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 224
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 240
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 256
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 272
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 288
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 159; y: 207
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 304
        hash: "3d1b648229210ae5b57a0be51cc02f67"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 159; y: 206
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 320
        hash: "3d1b648229210ae5b57a0be51cc02f67"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 159; y: 205
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 159; y: 203
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 336
        hash: "3d1b648229210ae5b57a0be51cc02f67"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 159; y: 199
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 157; y: 190
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 352
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 155; y: 176
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 153; y: 158
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 368
        hash: "57fa1d842d37df12004b493c1c5761f3"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 151; y: 141
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 148; y: 118
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 384
        hash: "521a8188877551a97cd3ea82d209e8ae"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 146; y: 94
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 141; y: 70
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 400
        hash: "ce126aaade1532e22a35416fd7203dde"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 136; y: 46
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 136; y: 46
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 416
        hash: "aa9c4301332240ccc00ec99a05b7f9c9"
    }
    Frame {
        msec: 432
        hash: "db0a670d61133a3420a3581ecb532773"
    }
    Frame {
        msec: 448
        hash: "b34de164d5ec0294ca27281e1e5e3cd6"
    }
    Frame {
        msec: 464
        hash: "8636af4591c61c4b4a548f3a38749413"
    }
    Frame {
        msec: 480
        hash: "eee4fa336149528dfb16565b856ca692"
    }
    Frame {
        msec: 496
        hash: "85eeaeaf359ed87417be68dc18c06d0c"
    }
    Frame {
        msec: 512
        hash: "d5db4af6cf35c61146bd24646d82ab83"
    }
    Frame {
        msec: 528
        hash: "2189fc03c337fe41f3d9f51929c9860f"
    }
    Frame {
        msec: 544
        hash: "4e3e283fb402dc4ec79f65878a513747"
    }
    Frame {
        msec: 560
        hash: "62f4281d8e049bc12b636b7ebe3862df"
    }
    Frame {
        msec: 576
        hash: "cf9a0a968459a1283fff91102eb29ba3"
    }
    Frame {
        msec: 592
        hash: "c432221928096cff3b76c8034db26b43"
    }
    Frame {
        msec: 608
        hash: "3df59808e56955c3c161609b72d93c7f"
    }
    Frame {
        msec: 624
        hash: "c497bcbe500905b8a69fd310fd7c7e1a"
    }
    Frame {
        msec: 640
        hash: "7dceef52fab6dc38d140e3097e39a271"
    }
    Frame {
        msec: 656
        hash: "c7bbd81b452db98fb8fd892762a23df6"
    }
    Frame {
        msec: 672
        hash: "17efc9793ef2966722544d561312b17a"
    }
    Frame {
        msec: 688
        hash: "1bf05b272ad6b8e5d134c94d9ba62030"
    }
    Frame {
        msec: 704
        hash: "cad61ba68fdfb26cfb136f22a2f8cc0c"
    }
    Frame {
        msec: 720
        hash: "0ce5ff1a1d9a6193ef763affa39cb790"
    }
    Frame {
        msec: 736
        hash: "880bce9130454aaf1261842b8f9b9a57"
    }
    Frame {
        msec: 752
        hash: "ab78cadac88156d9755d8b70d26384e8"
    }
    Frame {
        msec: 768
        hash: "4a22e502c105a7df0845ca75cbdfb0ec"
    }
    Frame {
        msec: 784
        hash: "d6209a0b9b9e0f2072179a4623c70fbd"
    }
    Frame {
        msec: 800
        hash: "85e85567831cf57df1f013f5bf3beb86"
    }
    Frame {
        msec: 816
        hash: "602d2e02029178faeb99748e2f70827e"
    }
    Frame {
        msec: 832
        hash: "fd4dbb6f47f6681af98eb6781ae7de58"
    }
    Frame {
        msec: 848
        hash: "faf3be40e402768724703f5d0051249f"
    }
    Frame {
        msec: 864
        hash: "bc650ca5b7a3bdc1f0f051b9481faf29"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 109; y: 69
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 880
        hash: "bc650ca5b7a3bdc1f0f051b9481faf29"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 110; y: 70
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 110; y: 71
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 896
        hash: "bc650ca5b7a3bdc1f0f051b9481faf29"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 110; y: 74
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 111; y: 79
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 912
        hash: "f2a679f2b7585245d4f1896fed4e0d1e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 111; y: 89
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 113; y: 104
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 928
        hash: "721b5fa42f583c1e1e1a751fc8aad270"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 115; y: 119
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 115; y: 135
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 944
        hash: "7e3ddefca9a99d6b9103ffd4524bc593"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 118; y: 160
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 120; y: 183
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 960
        hash: "7858d23cb4c206676eca51c1c09802b5"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 122; y: 205
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 124; y: 230
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 124; y: 230
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 976
        image: "flickable-vertical.1.png"
    }
    Frame {
        msec: 992
        hash: "e723da5ecaffe31f03b1d5ca6765229b"
    }
    Frame {
        msec: 1008
        hash: "73d169bf6bdfce801b824b7b560c3fad"
    }
    Frame {
        msec: 1024
        hash: "4e3e283fb402dc4ec79f65878a513747"
    }
    Frame {
        msec: 1040
        hash: "38c2e2835c20dbee55c69d0211a0be2d"
    }
    Frame {
        msec: 1056
        hash: "84e668ba374ff0004dd7222933a635cf"
    }
    Frame {
        msec: 1072
        hash: "349c7a84ff8f9b52d39dba1282353167"
    }
    Frame {
        msec: 1088
        hash: "b63218110c65b6d7b4bc2d63155204cd"
    }
    Frame {
        msec: 1104
        hash: "aad65a7070aa668dd8ce4a3cc0f0f117"
    }
    Frame {
        msec: 1120
        hash: "c4ae97e1d1f2efbc998f9b57c2373201"
    }
    Frame {
        msec: 1136
        hash: "94701ffaa4f45924ad89f92a30157c7d"
    }
    Frame {
        msec: 1152
        hash: "eee4fa336149528dfb16565b856ca692"
    }
    Frame {
        msec: 1168
        hash: "ff1a053c0af99c51353503002515843d"
    }
    Frame {
        msec: 1184
        hash: "118494c60034b0e265e28b34e3128d00"
    }
    Frame {
        msec: 1200
        hash: "bf693bffb37d7554a437eca21bdec7c1"
    }
    Frame {
        msec: 1216
        hash: "880f60263cd79fb6a1bff7252d2373bb"
    }
    Frame {
        msec: 1232
        hash: "b34de164d5ec0294ca27281e1e5e3cd6"
    }
    Frame {
        msec: 1248
        hash: "e1609c4e40fb9e043a9fff683b94c6c4"
    }
    Frame {
        msec: 1264
        hash: "2450b61b84c24727232c779114e6a474"
    }
    Frame {
        msec: 1280
        hash: "cf5ac4a5e3d42b3d4e171ed3227cfa85"
    }
    Frame {
        msec: 1296
        hash: "5cb5576ab347647ca881d4d450732df3"
    }
    Frame {
        msec: 1312
        hash: "34dc672ebfd75ec017d0c2f0bd435cd8"
    }
    Frame {
        msec: 1328
        hash: "aa9c4301332240ccc00ec99a05b7f9c9"
    }
    Frame {
        msec: 1344
        hash: "3f98121997a1613bd49d22003d1a1887"
    }
    Frame {
        msec: 1360
        hash: "86732d3e900877ae7a8615b7448afaaa"
    }
    Frame {
        msec: 1376
        hash: "7e2f2786d3c0540a0b6559fffe06ad3c"
    }
    Frame {
        msec: 1392
        hash: "79e00bbe77f0a178e8db30023a881c3f"
    }
    Frame {
        msec: 1408
        hash: "5f611226b3aa38f9aa3cd6a2dbd01f12"
    }
    Frame {
        msec: 1424
        hash: "4f4cd776b76272cfe79b86a108bd6b6e"
    }
    Frame {
        msec: 1440
        hash: "a746404a1a26e2a25b8d364dbef46eef"
    }
    Frame {
        msec: 1456
        hash: "9124d97d120de1806d86c8f437ec4ed2"
    }
    Frame {
        msec: 1472
        hash: "4fda328eafe6ec2d02d939517d6d82e3"
    }
    Frame {
        msec: 1488
        hash: "6afb6abe291c9e9628fd0b8c3da5d9db"
    }
    Frame {
        msec: 1504
        hash: "cb5962fe94c5d3ef754ff45f905a5c88"
    }
    Frame {
        msec: 1520
        hash: "57b5fc47ed700831b3dc3f2afbb1c3ed"
    }
    Frame {
        msec: 1536
        hash: "38793fb8a19c9566c8dd9d23c9a15b5d"
    }
    Frame {
        msec: 1552
        hash: "2e311a5dc484e9f4bc7bd85d32a693b1"
    }
    Frame {
        msec: 1568
        hash: "69d1eed68fba918e831899c8b84374a1"
    }
    Frame {
        msec: 1584
        hash: "c872391012e6ab2a6d1eb98c7f47f9e8"
    }
    Frame {
        msec: 1600
        hash: "cf12f90835d823550cd83d472b4f022f"
    }
    Frame {
        msec: 1616
        hash: "fbb2f03ddbd87ed419386eb2942bccac"
    }
    Frame {
        msec: 1632
        hash: "0788a0fdb51cedba0f8b597a4cc38ebe"
    }
    Frame {
        msec: 1648
        hash: "b6595edf06fba22f3258c9b433af6ab8"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 44; y: 282
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1664
        hash: "521a8188877551a97cd3ea82d209e8ae"
    }
    Frame {
        msec: 1680
        hash: "4d923cd520c00f5cd985283d62cf17ec"
    }
    Frame {
        msec: 1696
        hash: "7ccff14d344c7090fa634f6defd6511e"
    }
    Frame {
        msec: 1712
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 44; y: 282
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 1728
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1744
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1760
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1776
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1792
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1808
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1824
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1840
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1856
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1872
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1888
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1904
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1920
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1936
        image: "flickable-vertical.2.png"
    }
    Frame {
        msec: 1952
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1968
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 1984
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2000
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2016
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2032
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2048
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2064
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2080
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 95; y: 222
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 95; y: 221
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2096
        hash: "888c68103c4eef2f65ef32a93be8286a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 95; y: 220
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 95; y: 218
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2112
        hash: "888c68103c4eef2f65ef32a93be8286a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 95; y: 216
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 95; y: 212
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2128
        hash: "888c68103c4eef2f65ef32a93be8286a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 96; y: 205
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 96; y: 195
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2144
        hash: "888c68103c4eef2f65ef32a93be8286a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 97; y: 184
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 97; y: 168
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2160
        hash: "888c68103c4eef2f65ef32a93be8286a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 99; y: 153
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 99; y: 139
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2176
        hash: "888c68103c4eef2f65ef32a93be8286a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 101; y: 125
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 101; y: 112
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2192
        hash: "888c68103c4eef2f65ef32a93be8286a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 101; y: 99
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 101; y: 85
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2208
        hash: "888c68103c4eef2f65ef32a93be8286a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 103; y: 75
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 103; y: 62
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2224
        hash: "888c68103c4eef2f65ef32a93be8286a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 103; y: 53
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 103; y: 45
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 103; y: 45
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2240
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2256
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2272
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2288
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2304
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2320
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2336
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2352
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2368
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2384
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2400
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2416
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2432
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2448
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2464
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2480
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2496
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2512
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2528
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 90; y: 38
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2544
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 90; y: 39
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2560
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 90; y: 40
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 90; y: 41
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2576
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 91; y: 43
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 91; y: 46
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2592
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 92; y: 50
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 92; y: 55
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2608
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 94; y: 65
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 96; y: 79
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2624
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 97; y: 95
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 99; y: 112
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2640
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 101; y: 129
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 103; y: 148
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2656
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 105; y: 165
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 105; y: 180
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2672
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 107; y: 192
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 109; y: 205
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2688
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 109; y: 219
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 109; y: 230
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2704
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 111; y: 235
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 111; y: 238
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2720
        hash: "0d3bac7463b5fe7f585997e35f179122"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 111; y: 240
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 111; y: 240
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 2736
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2752
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2768
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2784
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2800
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2816
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2832
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2848
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2864
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2880
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2896
        image: "flickable-vertical.3.png"
    }
    Frame {
        msec: 2912
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2928
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2944
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2960
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2976
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 2992
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3008
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3024
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3040
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3056
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3072
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3088
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3104
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3120
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3136
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 43; y: 269
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3152
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3168
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3184
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3200
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3216
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 43; y: 269
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3232
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3248
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3264
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3280
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3296
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3312
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3328
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3344
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3360
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3376
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3392
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3408
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3424
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3440
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3456
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3472
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 75; y: 279
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3488
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3504
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3520
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3536
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 75; y: 279
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3552
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3568
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3584
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3600
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3616
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3632
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3648
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3664
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3680
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3696
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3712
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3728
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Frame {
        msec: 3744
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 116; y: 200
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3760
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 116; y: 199
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 117; y: 198
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3776
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 117; y: 195
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 117; y: 190
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3792
        hash: "998cb23307a61afefb59c8b9e361a89f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 118; y: 183
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 120; y: 166
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3808
        hash: "2e311a5dc484e9f4bc7bd85d32a693b1"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 122; y: 146
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 124; y: 123
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3824
        hash: "cbfcb7b986b0c51828473d98ca9fee03"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 126; y: 94
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 128; y: 67
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3840
        hash: "389b514c4cd4a4d65388608643d08c04"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 130; y: 41
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 133; y: 15
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3856
        image: "flickable-vertical.4.png"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 135; y: -6
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 138; y: -27
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3872
        hash: "cf9a0a968459a1283fff91102eb29ba3"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 140; y: -48
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 140; y: -48
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 3888
        hash: "77c86fb26126825cfd5b6ba21b903808"
    }
    Frame {
        msec: 3904
        hash: "c497bcbe500905b8a69fd310fd7c7e1a"
    }
    Frame {
        msec: 3920
        hash: "95bffb4d4aff1603e96af55cbc2dc3f2"
    }
    Frame {
        msec: 3936
        hash: "6fa87a7136528b688069fe1c4bd94043"
    }
    Frame {
        msec: 3952
        hash: "602c16e1382d810f853d647e531b4e8a"
    }
    Frame {
        msec: 3968
        hash: "01d1227e4f5b95f8b0c6a57a4b2314c4"
    }
    Frame {
        msec: 3984
        hash: "1db6401af45574b7453ad57766e60e6f"
    }
    Frame {
        msec: 4000
        hash: "067a1bef3df5d1c40842f28885d60250"
    }
    Frame {
        msec: 4016
        hash: "5fba31051e05ec00c0d68b8e8af94132"
    }
    Frame {
        msec: 4032
        hash: "d6209a0b9b9e0f2072179a4623c70fbd"
    }
    Frame {
        msec: 4048
        hash: "ec30f07ab0056a45954c07ecdfa1401a"
    }
    Frame {
        msec: 4064
        hash: "fef6c7767970a283bb3b13826f71bdac"
    }
    Frame {
        msec: 4080
        hash: "29621938e96be0d11c95fd1e4ca37631"
    }
    Frame {
        msec: 4096
        hash: "8103c96ac90ddf52056d7e8b32e4ae9e"
    }
    Frame {
        msec: 4112
        hash: "d72bf8b88efe603050ad038380173969"
    }
    Frame {
        msec: 4128
        hash: "4438b56eb6aa800602634db6016caa50"
    }
    Frame {
        msec: 4144
        hash: "44674f7a874023c3932d698344ccda0e"
    }
    Frame {
        msec: 4160
        hash: "155a834ddaa7128b6f5a2a406b340315"
    }
    Frame {
        msec: 4176
        hash: "3886efa510581ee5b6c4a2ed76aeb42d"
    }
    Frame {
        msec: 4192
        hash: "094954e8d10b85d3941626dec4fb36af"
    }
    Frame {
        msec: 4208
        hash: "b597aeb20a8630e4b1dfd0a7be383e4d"
    }
    Frame {
        msec: 4224
        hash: "abc58e74ab197a2d7c243ddd67442e53"
    }
    Frame {
        msec: 4240
        hash: "b6ec106d39af13492c3d43bf006b7b15"
    }
    Frame {
        msec: 4256
        hash: "d80211f898473a01e0c0641b96bc92f4"
    }
    Frame {
        msec: 4272
        hash: "5010579fcd925e65c778c2e9cf0317de"
    }
    Frame {
        msec: 4288
        hash: "5010579fcd925e65c778c2e9cf0317de"
    }
    Frame {
        msec: 4304
        hash: "d80211f898473a01e0c0641b96bc92f4"
    }
    Frame {
        msec: 4320
        hash: "27cfc811f62029df48ea7f371ff5654b"
    }
    Frame {
        msec: 4336
        hash: "b6ec106d39af13492c3d43bf006b7b15"
    }
    Frame {
        msec: 4352
        hash: "28c8e3f08f46bf13cc52a7d6a31a7cf1"
    }
    Frame {
        msec: 4368
        hash: "b597aeb20a8630e4b1dfd0a7be383e4d"
    }
    Frame {
        msec: 4384
        hash: "a3a3682ce0d2a2d57457458b13645afa"
    }
    Frame {
        msec: 4400
        hash: "98bf25cbb8202fe1576ac15bac7b9e65"
    }
    Frame {
        msec: 4416
        hash: "16b99c9cf5297a5251869a3935084cf7"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 136; y: 176
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4432
        hash: "16b99c9cf5297a5251869a3935084cf7"
    }
    Frame {
        msec: 4448
        hash: "16b99c9cf5297a5251869a3935084cf7"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 136; y: 175
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4464
        hash: "16b99c9cf5297a5251869a3935084cf7"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 136; y: 173
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 136; y: 168
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4480
        hash: "155a834ddaa7128b6f5a2a406b340315"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 134; y: 159
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 133; y: 142
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4496
        hash: "abc58e74ab197a2d7c243ddd67442e53"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 130; y: 119
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 128; y: 98
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4512
        hash: "e5c5b741da7c028ec77f52016675c1ca"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 126; y: 78
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 124; y: 59
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4528
        hash: "12481bcccb524a478851a57d4db6cf8d"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 122; y: 44
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 120; y: 30
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4544
        hash: "a49985bd332cd3376986d379c474a3de"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 120; y: 21
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 118; y: 12
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 118; y: 12
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4560
        hash: "cd4e55b15e9df7fee1862180fddec0ca"
    }
    Frame {
        msec: 4576
        hash: "64ff54775d198b616597f4539de90bd8"
    }
    Frame {
        msec: 4592
        hash: "2b188745bfff51f9d3af90b7ad9c8d77"
    }
    Frame {
        msec: 4608
        hash: "2dde7d565f92f22c6524448f97107e35"
    }
    Frame {
        msec: 4624
        hash: "897a454ac464008d6dd7864eb608ae65"
    }
    Frame {
        msec: 4640
        hash: "269df4f1aca4f0cdbd5c86c2e115bd3c"
    }
    Frame {
        msec: 4656
        hash: "ec0ebdbd3f4665fba7f6a523a82a5071"
    }
    Frame {
        msec: 4672
        hash: "c1ac6a385f580f23b3486c643d276e33"
    }
    Frame {
        msec: 4688
        hash: "3de0d147a6a3c1382ec64a80996bb4f4"
    }
    Frame {
        msec: 4704
        hash: "8db942b5909f63d4369ad5b29938ef49"
    }
    Frame {
        msec: 4720
        hash: "f7840636f2d01c25be8e9c77230cca53"
    }
    Frame {
        msec: 4736
        hash: "d315f82e175361fed83193ce550cb6e9"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 111; y: 67
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4752
        hash: "d315f82e175361fed83193ce550cb6e9"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 111; y: 70
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 111; y: 74
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4768
        hash: "155a834ddaa7128b6f5a2a406b340315"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 111; y: 79
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 112; y: 86
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4784
        hash: "00b072a0adbfcd520d495ef6540f5680"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 112; y: 95
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 114; y: 105
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4800
        hash: "fb605e95988a6110384671e7f3f18ad8"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 114; y: 115
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 115; y: 126
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4816
        image: "flickable-vertical.5.png"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 115; y: 142
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 117; y: 159
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4832
        hash: "4d1eb644b592a693b13fe14377aeed97"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 120; y: 180
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 122; y: 202
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4848
        hash: "00eb1d3b016eb0220461074ce81b1aef"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 127; y: 224
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 129; y: 243
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 129; y: 243
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 4864
        hash: "77c86fb26126825cfd5b6ba21b903808"
    }
    Frame {
        msec: 4880
        hash: "e80f024bbdce0ceeae137e347abc95a4"
    }
    Frame {
        msec: 4896
        hash: "bb189f39a836b9a2aa68f4535ed1d6fb"
    }
    Frame {
        msec: 4912
        hash: "cf9a0a968459a1283fff91102eb29ba3"
    }
    Frame {
        msec: 4928
        hash: "27130e7f6b853a287a7bdd8608628a4f"
    }
    Frame {
        msec: 4944
        hash: "231c7b7078af00a36cfee3d5e43a4021"
    }
    Frame {
        msec: 4960
        hash: "d8ffc8cc9cecc25cb9b4e7990fb7b8e7"
    }
    Frame {
        msec: 4976
        hash: "fb5db5dafdb375132f1f1a461193bc60"
    }
    Frame {
        msec: 4992
        hash: "64100f9f102ffc9415e306c087547709"
    }
    Frame {
        msec: 5008
        hash: "6960e5c4feb55043ff91934fc934734e"
    }
    Frame {
        msec: 5024
        hash: "349c7a84ff8f9b52d39dba1282353167"
    }
    Frame {
        msec: 5040
        hash: "bb41010df844312fc15bb5b42712619a"
    }
    Frame {
        msec: 5056
        hash: "63a3e18670bb2a5e7edfe3b752c0a1b5"
    }
    Frame {
        msec: 5072
        hash: "92b1d0fbadbefe9f122b14903a5e0ee9"
    }
    Frame {
        msec: 5088
        hash: "6b979e1a4bc7226a89ffb97be2f08147"
    }
    Frame {
        msec: 5104
        hash: "7b783908e0b10d329a7d3172f2302a85"
    }
    Frame {
        msec: 5120
        hash: "41d5ef3390cfc0d806825fc0cd033be6"
    }
    Frame {
        msec: 5136
        hash: "ff1a053c0af99c51353503002515843d"
    }
    Frame {
        msec: 5152
        hash: "63b26ecde2a2a9ce36884191304352ed"
    }
    Frame {
        msec: 5168
        hash: "bdcff2f9f2c376974211ea6ad5c4961f"
    }
    Frame {
        msec: 5184
        hash: "00ffef1a1d4341ac1c7f43d493a9e826"
    }
    Frame {
        msec: 5200
        hash: "65dcbb543656f65267c7d32dcd644e56"
    }
    Frame {
        msec: 5216
        hash: "38b49419b7103d76da2b6d7101d63d88"
    }
    Frame {
        msec: 5232
        hash: "de39f6bf64745054cbee30ddf306f641"
    }
    Frame {
        msec: 5248
        hash: "d6b5ceca4aa48a7d4fd901d44c151b53"
    }
    Frame {
        msec: 5264
        hash: "876e6eee8a35c34e2dd5269f86a9ab3a"
    }
    Frame {
        msec: 5280
        hash: "f94219306eac2e678881d0b607d15a1e"
    }
    Frame {
        msec: 5296
        hash: "c9184196ef45c985f08f80435492641d"
    }
    Frame {
        msec: 5312
        hash: "34dc672ebfd75ec017d0c2f0bd435cd8"
    }
    Frame {
        msec: 5328
        hash: "4daf1c730fdf13e0a87b28208f2b6dd1"
    }
    Frame {
        msec: 5344
        hash: "c28d5d7d9d3a86e5bbf6ad48331f9c61"
    }
    Frame {
        msec: 5360
        hash: "3f98121997a1613bd49d22003d1a1887"
    }
    Frame {
        msec: 5376
        hash: "86732d3e900877ae7a8615b7448afaaa"
    }
    Frame {
        msec: 5392
        hash: "9f3da7ebaeb319c9fec0abdd6bd76ee2"
    }
    Frame {
        msec: 5408
        hash: "326563c2c812a74c7f1fa5e9da0c2369"
    }
    Frame {
        msec: 5424
        hash: "79e00bbe77f0a178e8db30023a881c3f"
    }
    Frame {
        msec: 5440
        hash: "e624204566550e928ab2a2c54113d217"
    }
    Frame {
        msec: 5456
        hash: "b95bf705b81544b05f560c54dec56ff1"
    }
    Frame {
        msec: 5472
        hash: "4f4cd776b76272cfe79b86a108bd6b6e"
    }
    Frame {
        msec: 5488
        hash: "ec2eb1b39a252bd9b37d12ede3d231ce"
    }
    Frame {
        msec: 5504
        hash: "a746404a1a26e2a25b8d364dbef46eef"
    }
    Frame {
        msec: 5520
        hash: "17d190465ee0d348d9b67a748626d99e"
    }
    Frame {
        msec: 5536
        hash: "9124d97d120de1806d86c8f437ec4ed2"
    }
    Frame {
        msec: 5552
        hash: "ea746de2380835d299c56bb01f0aa83c"
    }
    Frame {
        msec: 5568
        hash: "4fda328eafe6ec2d02d939517d6d82e3"
    }
    Frame {
        msec: 5584
        hash: "9c6f671def0b1f5d780024a9dad439e6"
    }
    Frame {
        msec: 5600
        hash: "b7d441d0bb27ed6d1984f324b6e02548"
    }
    Frame {
        msec: 5616
        hash: "3042a62a1125171d9530b696f4b36e19"
    }
    Frame {
        msec: 5632
        hash: "4534f40cf6bb7f402d7252c474629664"
    }
    Frame {
        msec: 5648
        hash: "cb5962fe94c5d3ef754ff45f905a5c88"
    }
    Frame {
        msec: 5664
        hash: "b5a5f9f3aa0948f0bd8d9b4a3fceae50"
    }
    Frame {
        msec: 5680
        hash: "2e0605899abb5725cf22561ec9293879"
    }
    Frame {
        msec: 5696
        hash: "1f260f1d931326be7e398f7c87e44735"
    }
    Frame {
        msec: 5712
        hash: "57b5fc47ed700831b3dc3f2afbb1c3ed"
    }
    Frame {
        msec: 5728
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5744
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5760
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5776
        image: "flickable-vertical.6.png"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 102; y: 279
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 5792
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5808
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5824
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5840
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 102; y: 279
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 5856
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5872
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5888
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5904
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5920
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5936
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5952
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5968
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 5984
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6000
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6016
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6032
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6048
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6064
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 148; y: 276
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6080
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6096
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6112
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6128
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6144
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 148; y: 276
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6160
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6176
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6192
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6208
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6224
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6240
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6256
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6272
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6288
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6304
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6320
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6336
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6352
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6368
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Frame {
        msec: 6384
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 129; y: 101
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6400
        hash: "c18aeb6fb3914a0be2d34ff76249ed8e"
    }
    Frame {
        msec: 6416
        hash: "c18aeb6fb3914a0be2d34ff76249ed8e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 129; y: 103
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 129; y: 105
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6432
        hash: "c18aeb6fb3914a0be2d34ff76249ed8e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 130; y: 110
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 132; y: 123
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6448
        hash: "8b9167c04a8acc7f8ade258a3e58893b"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 132; y: 133
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 134; y: 145
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6464
        hash: "a5daa2f6c932fa38038639bdc8231c5d"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 136; y: 159
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 138; y: 172
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6480
        hash: "f342612efcd5e0820b44bd788ec52d7a"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 138; y: 187
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 140; y: 203
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6496
        hash: "9a66e65c69ec833a36cce5cbd7d8257f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 140; y: 214
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 141; y: 224
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6512
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 143; y: 235
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 143; y: 246
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6528
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 143; y: 257
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 145; y: 269
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6544
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 145; y: 278
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 145; y: 289
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6560
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 147; y: 299
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 147; y: 308
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6576
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 149; y: 316
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 149; y: 318
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6592
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 149; y: 320
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 149; y: 321
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 149; y: 321
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 6608
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6624
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6640
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6656
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6672
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6688
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6704
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6720
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6736
        image: "flickable-vertical.7.png"
    }
    Frame {
        msec: 6752
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6768
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6784
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6800
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6816
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6832
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6848
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6864
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6880
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6896
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6912
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6928
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6944
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6960
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6976
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Frame {
        msec: 6992
        hash: "bca482a77823f03e8fb4170ee329fc0e"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 166; y: 191
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7008
        hash: "9ed65a21e4aaedf9c48a38324b3f5480"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 167; y: 190
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 167; y: 189
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7024
        hash: "9ed65a21e4aaedf9c48a38324b3f5480"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 167; y: 188
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 168; y: 185
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7040
        hash: "9ed65a21e4aaedf9c48a38324b3f5480"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 169; y: 183
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 169; y: 179
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7056
        hash: "c4925926f64b852ff6c8d07e1c70ead5"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 170; y: 172
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 170; y: 162
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7072
        hash: "da771cedad067b8f25c100613b6a14f2"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 168; y: 150
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 167; y: 139
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7088
        hash: "c8862bf76a431edc1cf04f4114fa859f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 165; y: 125
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 163; y: 113
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7104
        hash: "4d923cd520c00f5cd985283d62cf17ec"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 161; y: 103
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 160; y: 92
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7120
        hash: "76b0d1c77ba29bad836673b1b79de911"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 158; y: 80
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 156; y: 66
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7136
        hash: "3f9c5686f0a9ef5ecf2b8338ef2e7933"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 154; y: 52
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 154; y: 38
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7152
        hash: "799d83eedefa0a56f37a83404c59ad4f"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 152; y: 27
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 150; y: 18
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 150; y: 18
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7168
        hash: "d6b5ceca4aa48a7d4fd901d44c151b53"
    }
    Frame {
        msec: 7184
        hash: "e1609c4e40fb9e043a9fff683b94c6c4"
    }
    Frame {
        msec: 7200
        hash: "ea457fc4d4065d2ed0e9f6efc47a06ee"
    }
    Frame {
        msec: 7216
        hash: "b7f4319aa9c21640a697ee89f162bb49"
    }
    Frame {
        msec: 7232
        hash: "880f60263cd79fb6a1bff7252d2373bb"
    }
    Frame {
        msec: 7248
        hash: "00ffef1a1d4341ac1c7f43d493a9e826"
    }
    Frame {
        msec: 7264
        hash: "c949fe87ba91e08f26a1c4d90028513f"
    }
    Frame {
        msec: 7280
        hash: "8636af4591c61c4b4a548f3a38749413"
    }
    Frame {
        msec: 7296
        hash: "63b26ecde2a2a9ce36884191304352ed"
    }
    Frame {
        msec: 7312
        hash: "843f7263f63442f0041bf2c1a6fae400"
    }
    Frame {
        msec: 7328
        hash: "ff1a053c0af99c51353503002515843d"
    }
    Frame {
        msec: 7344
        hash: "47aea3ac4ea935d43f731a258287c2e8"
    }
    Frame {
        msec: 7360
        hash: "eee4fa336149528dfb16565b856ca692"
    }
    Frame {
        msec: 7376
        hash: "bb94493c25c56c41e81ef1e390adf63d"
    }
    Frame {
        msec: 7392
        hash: "2915f455a5e1e8c6b8cc78309c5e84d9"
    }
    Frame {
        msec: 7408
        hash: "94701ffaa4f45924ad89f92a30157c7d"
    }
    Frame {
        msec: 7424
        hash: "92fae8cf4b8d8404b26a31f995860b95"
    }
    Frame {
        msec: 7440
        hash: "6b979e1a4bc7226a89ffb97be2f08147"
    }
    Frame {
        msec: 7456
        hash: "dd94beeb0b4933a9ac2236a9abe630ff"
    }
    Mouse {
        type: 2
        button: 1
        buttons: 1
        x: 109; y: 172
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7472
        hash: "dd94beeb0b4933a9ac2236a9abe630ff"
    }
    Frame {
        msec: 7488
        hash: "dd94beeb0b4933a9ac2236a9abe630ff"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 109; y: 170
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 109; y: 168
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7504
        hash: "dd94beeb0b4933a9ac2236a9abe630ff"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 109; y: 165
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 109; y: 152
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7520
        hash: "34c7ed1c072d84626a8a64f7db02f71d"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 109; y: 135
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 109; y: 116
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7536
        hash: "e723da5ecaffe31f03b1d5ca6765229b"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 106; y: 91
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 104; y: 66
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7552
        hash: "a85128cae494abeb5d45ab8df0d127a6"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 102; y: 42
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 100; y: 17
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7568
        hash: "3599a92966c27321e9f702f3428b9b00"
    }
    Mouse {
        type: 5
        button: 0
        buttons: 1
        x: 100; y: -2
        modifiers: 0
        sendToViewport: true
    }
    Mouse {
        type: 3
        button: 1
        buttons: 0
        x: 100; y: -2
        modifiers: 0
        sendToViewport: true
    }
    Frame {
        msec: 7584
        hash: "067a1bef3df5d1c40842f28885d60250"
    }
    Frame {
        msec: 7600
        hash: "82f818ed44a191fb51e637b8068786dc"
    }
    Frame {
        msec: 7616
        hash: "f408f59707195549ba61f030a3f020cd"
    }
    Frame {
        msec: 7632
        hash: "66e79c8b2f8e3a57c3bc14935c5df7d1"
    }
    Frame {
        msec: 7648
        hash: "4341c6b7b0d2e8021b51cb1abab85e10"
    }
    Frame {
        msec: 7664
        hash: "5ec8ee5ccecac1787b2f5e99268e810d"
    }
    Frame {
        msec: 7680
        hash: "1fae7b735ff6e88abfb1423f8960da4f"
    }
    Frame {
        msec: 7696
        image: "flickable-vertical.8.png"
    }
    Frame {
        msec: 7712
        hash: "dce74ff07eb37c82a38b3e515f9a43f2"
    }
    Frame {
        msec: 7728
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7744
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7760
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7776
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7792
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7808
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7824
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7840
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7856
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7872
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7888
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7904
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7920
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7936
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7952
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7968
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 7984
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8000
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8016
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8032
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8048
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8064
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8080
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8096
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8112
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8128
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8144
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8160
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8176
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8192
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8208
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8224
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8240
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8256
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8272
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8288
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8304
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8320
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8336
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8352
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8368
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8384
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8400
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
    Frame {
        msec: 8416
        hash: "ba2c06129f17fde474427859d66ecd23"
    }
}
