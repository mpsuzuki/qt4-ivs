import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "wrap.0.png"
    }
    Key {
        type: 6
        key: 84
        modifiers: 33554432
        text: "54"
        autorep: false
        count: 1
    }
    Frame {
        msec: 32
        hash: "3e34b9a8c5df08caa18f37289c25138f"
    }
    Key {
        type: 7
        key: 16777248
        modifiers: 33554432
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 48
        hash: "3e34b9a8c5df08caa18f37289c25138f"
    }
    Frame {
        msec: 64
        hash: "3e34b9a8c5df08caa18f37289c25138f"
    }
    Key {
        type: 7
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 80
        hash: "3e34b9a8c5df08caa18f37289c25138f"
    }
    Frame {
        msec: 96
        hash: "3e34b9a8c5df08caa18f37289c25138f"
    }
    Key {
        type: 6
        key: 72
        modifiers: 0
        text: "68"
        autorep: false
        count: 1
    }
    Frame {
        msec: 112
        hash: "4242081446f2a3122bbd4f8c03a67e5c"
    }
    Frame {
        msec: 128
        hash: "4242081446f2a3122bbd4f8c03a67e5c"
    }
    Frame {
        msec: 144
        hash: "4242081446f2a3122bbd4f8c03a67e5c"
    }
    Key {
        type: 6
        key: 73
        modifiers: 0
        text: "69"
        autorep: false
        count: 1
    }
    Frame {
        msec: 160
        hash: "79c4a9defe89f99b3f6b3c25bd81fc7e"
    }
    Frame {
        msec: 176
        hash: "79c4a9defe89f99b3f6b3c25bd81fc7e"
    }
    Frame {
        msec: 192
        hash: "79c4a9defe89f99b3f6b3c25bd81fc7e"
    }
    Key {
        type: 7
        key: 72
        modifiers: 0
        text: "68"
        autorep: false
        count: 1
    }
    Frame {
        msec: 208
        hash: "79c4a9defe89f99b3f6b3c25bd81fc7e"
    }
    Frame {
        msec: 224
        hash: "79c4a9defe89f99b3f6b3c25bd81fc7e"
    }
    Key {
        type: 6
        key: 83
        modifiers: 0
        text: "73"
        autorep: false
        count: 1
    }
    Frame {
        msec: 240
        hash: "0bee22de7793974cadec12dfb5df16aa"
    }
    Key {
        type: 7
        key: 73
        modifiers: 0
        text: "69"
        autorep: false
        count: 1
    }
    Frame {
        msec: 256
        hash: "0bee22de7793974cadec12dfb5df16aa"
    }
    Frame {
        msec: 272
        hash: "0bee22de7793974cadec12dfb5df16aa"
    }
    Frame {
        msec: 288
        hash: "0bee22de7793974cadec12dfb5df16aa"
    }
    Frame {
        msec: 304
        hash: "0bee22de7793974cadec12dfb5df16aa"
    }
    Key {
        type: 7
        key: 83
        modifiers: 0
        text: "73"
        autorep: false
        count: 1
    }
    Frame {
        msec: 320
        hash: "0bee22de7793974cadec12dfb5df16aa"
    }
    Frame {
        msec: 336
        hash: "0bee22de7793974cadec12dfb5df16aa"
    }
    Key {
        type: 6
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 352
        hash: "07393d1c1bb6da436700881ebcd38195"
    }
    Frame {
        msec: 368
        hash: "07393d1c1bb6da436700881ebcd38195"
    }
    Frame {
        msec: 384
        hash: "07393d1c1bb6da436700881ebcd38195"
    }
    Frame {
        msec: 400
        hash: "07393d1c1bb6da436700881ebcd38195"
    }
    Key {
        type: 7
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 416
        hash: "07393d1c1bb6da436700881ebcd38195"
    }
    Frame {
        msec: 432
        hash: "07393d1c1bb6da436700881ebcd38195"
    }
    Frame {
        msec: 448
        hash: "07393d1c1bb6da436700881ebcd38195"
    }
    Frame {
        msec: 464
        hash: "07393d1c1bb6da436700881ebcd38195"
    }
    Key {
        type: 6
        key: 73
        modifiers: 0
        text: "69"
        autorep: false
        count: 1
    }
    Frame {
        msec: 480
        hash: "b12a4550ae068d157d340c008047d6f1"
    }
    Frame {
        msec: 496
        hash: "b12a4550ae068d157d340c008047d6f1"
    }
    Frame {
        msec: 512
        hash: "b12a4550ae068d157d340c008047d6f1"
    }
    Frame {
        msec: 528
        hash: "b12a4550ae068d157d340c008047d6f1"
    }
    Key {
        type: 6
        key: 83
        modifiers: 0
        text: "73"
        autorep: false
        count: 1
    }
    Key {
        type: 7
        key: 73
        modifiers: 0
        text: "69"
        autorep: false
        count: 1
    }
    Frame {
        msec: 544
        hash: "f291de0963549b92d607f38d2d08c551"
    }
    Frame {
        msec: 560
        hash: "f291de0963549b92d607f38d2d08c551"
    }
    Frame {
        msec: 576
        hash: "f291de0963549b92d607f38d2d08c551"
    }
    Frame {
        msec: 592
        hash: "f291de0963549b92d607f38d2d08c551"
    }
    Frame {
        msec: 608
        hash: "f291de0963549b92d607f38d2d08c551"
    }
    Key {
        type: 7
        key: 83
        modifiers: 0
        text: "73"
        autorep: false
        count: 1
    }
    Key {
        type: 6
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 624
        hash: "b7eedae59cc521aa8222596cd97bf129"
    }
    Frame {
        msec: 640
        hash: "b7eedae59cc521aa8222596cd97bf129"
    }
    Frame {
        msec: 656
        hash: "b7eedae59cc521aa8222596cd97bf129"
    }
    Frame {
        msec: 672
        hash: "b7eedae59cc521aa8222596cd97bf129"
    }
    Key {
        type: 7
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 688
        hash: "b7eedae59cc521aa8222596cd97bf129"
    }
    Frame {
        msec: 704
        hash: "b7eedae59cc521aa8222596cd97bf129"
    }
    Key {
        type: 6
        key: 65
        modifiers: 0
        text: "61"
        autorep: false
        count: 1
    }
    Frame {
        msec: 720
        hash: "98ef281d984841075f2fc82cebcba3a9"
    }
    Frame {
        msec: 736
        hash: "98ef281d984841075f2fc82cebcba3a9"
    }
    Frame {
        msec: 752
        hash: "98ef281d984841075f2fc82cebcba3a9"
    }
    Frame {
        msec: 768
        hash: "98ef281d984841075f2fc82cebcba3a9"
    }
    Frame {
        msec: 784
        hash: "98ef281d984841075f2fc82cebcba3a9"
    }
    Key {
        type: 7
        key: 65
        modifiers: 0
        text: "61"
        autorep: false
        count: 1
    }
    Frame {
        msec: 800
        hash: "98ef281d984841075f2fc82cebcba3a9"
    }
    Key {
        type: 6
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 816
        hash: "e7b8f24ba55765e2fc1f386d510b402f"
    }
    Frame {
        msec: 832
        hash: "e7b8f24ba55765e2fc1f386d510b402f"
    }
    Frame {
        msec: 848
        hash: "e7b8f24ba55765e2fc1f386d510b402f"
    }
    Key {
        type: 7
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 864
        hash: "e7b8f24ba55765e2fc1f386d510b402f"
    }
    Frame {
        msec: 880
        hash: "e7b8f24ba55765e2fc1f386d510b402f"
    }
    Frame {
        msec: 896
        hash: "e7b8f24ba55765e2fc1f386d510b402f"
    }
    Key {
        type: 6
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 912
        hash: "38a3062cb4f23993416f83ff6acbe189"
    }
    Frame {
        msec: 928
        hash: "38a3062cb4f23993416f83ff6acbe189"
    }
    Frame {
        msec: 944
        hash: "38a3062cb4f23993416f83ff6acbe189"
    }
    Frame {
        msec: 960
        hash: "38a3062cb4f23993416f83ff6acbe189"
    }
    Frame {
        msec: 976
        image: "wrap.1.png"
    }
    Frame {
        msec: 992
        hash: "38a3062cb4f23993416f83ff6acbe189"
    }
    Key {
        type: 6
        key: 69
        modifiers: 0
        text: "65"
        autorep: false
        count: 1
    }
    Key {
        type: 7
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1008
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Frame {
        msec: 1024
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Frame {
        msec: 1040
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Frame {
        msec: 1056
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Frame {
        msec: 1072
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Key {
        type: 7
        key: 69
        modifiers: 0
        text: "65"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1088
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Frame {
        msec: 1104
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Frame {
        msec: 1120
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Frame {
        msec: 1136
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Frame {
        msec: 1152
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Frame {
        msec: 1168
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Frame {
        msec: 1184
        hash: "daef9995a008f0ca672adc98315a6b9f"
    }
    Key {
        type: 6
        key: 83
        modifiers: 0
        text: "73"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1200
        hash: "da27d35f241ccc7c1ee2832e491fa726"
    }
    Frame {
        msec: 1216
        hash: "da27d35f241ccc7c1ee2832e491fa726"
    }
    Frame {
        msec: 1232
        hash: "da27d35f241ccc7c1ee2832e491fa726"
    }
    Frame {
        msec: 1248
        hash: "da27d35f241ccc7c1ee2832e491fa726"
    }
    Frame {
        msec: 1264
        hash: "da27d35f241ccc7c1ee2832e491fa726"
    }
    Key {
        type: 7
        key: 83
        modifiers: 0
        text: "73"
        autorep: false
        count: 1
    }
    Key {
        type: 6
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1280
        hash: "7136f5cfcca4a86b8764667895efa813"
    }
    Frame {
        msec: 1296
        hash: "7136f5cfcca4a86b8764667895efa813"
    }
    Key {
        type: 6
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1312
        hash: "b99aec3d97f4442378a18ac88d50b97d"
    }
    Frame {
        msec: 1328
        hash: "b99aec3d97f4442378a18ac88d50b97d"
    }
    Frame {
        msec: 1344
        hash: "b99aec3d97f4442378a18ac88d50b97d"
    }
    Key {
        type: 7
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1360
        hash: "b99aec3d97f4442378a18ac88d50b97d"
    }
    Frame {
        msec: 1376
        hash: "b99aec3d97f4442378a18ac88d50b97d"
    }
    Key {
        type: 7
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1392
        hash: "b99aec3d97f4442378a18ac88d50b97d"
    }
    Frame {
        msec: 1408
        hash: "b99aec3d97f4442378a18ac88d50b97d"
    }
    Frame {
        msec: 1424
        hash: "b99aec3d97f4442378a18ac88d50b97d"
    }
    Frame {
        msec: 1440
        hash: "b99aec3d97f4442378a18ac88d50b97d"
    }
    Key {
        type: 6
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1456
        hash: "c32293903502fd1964cfbc10515b2ef7"
    }
    Frame {
        msec: 1472
        hash: "c32293903502fd1964cfbc10515b2ef7"
    }
    Frame {
        msec: 1488
        hash: "c32293903502fd1964cfbc10515b2ef7"
    }
    Frame {
        msec: 1504
        hash: "c32293903502fd1964cfbc10515b2ef7"
    }
    Frame {
        msec: 1520
        hash: "c32293903502fd1964cfbc10515b2ef7"
    }
    Key {
        type: 7
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1536
        hash: "c32293903502fd1964cfbc10515b2ef7"
    }
    Frame {
        msec: 1552
        hash: "c32293903502fd1964cfbc10515b2ef7"
    }
    Key {
        type: 6
        key: 72
        modifiers: 0
        text: "68"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1568
        hash: "47371eb93a2a8fac7afb53990fac9130"
    }
    Frame {
        msec: 1584
        hash: "47371eb93a2a8fac7afb53990fac9130"
    }
    Frame {
        msec: 1600
        hash: "47371eb93a2a8fac7afb53990fac9130"
    }
    Frame {
        msec: 1616
        hash: "47371eb93a2a8fac7afb53990fac9130"
    }
    Frame {
        msec: 1632
        hash: "47371eb93a2a8fac7afb53990fac9130"
    }
    Key {
        type: 6
        key: 65
        modifiers: 0
        text: "61"
        autorep: false
        count: 1
    }
    Key {
        type: 7
        key: 72
        modifiers: 0
        text: "68"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1648
        hash: "5c22c2566b437497dd6fd908135ec39e"
    }
    Frame {
        msec: 1664
        hash: "5c22c2566b437497dd6fd908135ec39e"
    }
    Frame {
        msec: 1680
        hash: "5c22c2566b437497dd6fd908135ec39e"
    }
    Frame {
        msec: 1696
        hash: "5c22c2566b437497dd6fd908135ec39e"
    }
    Frame {
        msec: 1712
        hash: "5c22c2566b437497dd6fd908135ec39e"
    }
    Key {
        type: 6
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1728
        hash: "29b4e69de4c83ccdee6ef116ab3785ee"
    }
    Frame {
        msec: 1744
        hash: "29b4e69de4c83ccdee6ef116ab3785ee"
    }
    Frame {
        msec: 1760
        hash: "29b4e69de4c83ccdee6ef116ab3785ee"
    }
    Key {
        type: 7
        key: 65
        modifiers: 0
        text: "61"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1776
        hash: "29b4e69de4c83ccdee6ef116ab3785ee"
    }
    Key {
        type: 6
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1792
        hash: "5ab8ecb0ca9fed70f1d8add6b7b3972d"
    }
    Frame {
        msec: 1808
        hash: "5ab8ecb0ca9fed70f1d8add6b7b3972d"
    }
    Key {
        type: 7
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1824
        hash: "5ab8ecb0ca9fed70f1d8add6b7b3972d"
    }
    Frame {
        msec: 1840
        hash: "5ab8ecb0ca9fed70f1d8add6b7b3972d"
    }
    Frame {
        msec: 1856
        hash: "5ab8ecb0ca9fed70f1d8add6b7b3972d"
    }
    Frame {
        msec: 1872
        hash: "5ab8ecb0ca9fed70f1d8add6b7b3972d"
    }
    Key {
        type: 7
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1888
        hash: "5ab8ecb0ca9fed70f1d8add6b7b3972d"
    }
    Frame {
        msec: 1904
        hash: "5ab8ecb0ca9fed70f1d8add6b7b3972d"
    }
    Frame {
        msec: 1920
        hash: "5ab8ecb0ca9fed70f1d8add6b7b3972d"
    }
    Frame {
        msec: 1936
        image: "wrap.2.png"
    }
    Frame {
        msec: 1952
        hash: "5ab8ecb0ca9fed70f1d8add6b7b3972d"
    }
    Key {
        type: 6
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1968
        hash: "2a43f5ac0c7bdf38e367b0cdb0bccea9"
    }
    Frame {
        msec: 1984
        hash: "2a43f5ac0c7bdf38e367b0cdb0bccea9"
    }
    Frame {
        msec: 2000
        hash: "2a43f5ac0c7bdf38e367b0cdb0bccea9"
    }
    Frame {
        msec: 2016
        hash: "2a43f5ac0c7bdf38e367b0cdb0bccea9"
    }
    Frame {
        msec: 2032
        hash: "2a43f5ac0c7bdf38e367b0cdb0bccea9"
    }
    Frame {
        msec: 2048
        hash: "2a43f5ac0c7bdf38e367b0cdb0bccea9"
    }
    Key {
        type: 6
        key: 69
        modifiers: 0
        text: "65"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2064
        hash: "0f28c7855c7fde3390d16a2638e23bd0"
    }
    Key {
        type: 7
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2080
        hash: "0f28c7855c7fde3390d16a2638e23bd0"
    }
    Frame {
        msec: 2096
        hash: "0f28c7855c7fde3390d16a2638e23bd0"
    }
    Frame {
        msec: 2112
        hash: "0f28c7855c7fde3390d16a2638e23bd0"
    }
    Key {
        type: 7
        key: 69
        modifiers: 0
        text: "65"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2128
        hash: "0f28c7855c7fde3390d16a2638e23bd0"
    }
    Frame {
        msec: 2144
        hash: "0f28c7855c7fde3390d16a2638e23bd0"
    }
    Frame {
        msec: 2160
        hash: "0f28c7855c7fde3390d16a2638e23bd0"
    }
    Frame {
        msec: 2176
        hash: "0f28c7855c7fde3390d16a2638e23bd0"
    }
    Frame {
        msec: 2192
        hash: "0f28c7855c7fde3390d16a2638e23bd0"
    }
    Frame {
        msec: 2208
        hash: "0f28c7855c7fde3390d16a2638e23bd0"
    }
    Key {
        type: 6
        key: 88
        modifiers: 0
        text: "78"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2224
        hash: "b56e002e5eddde0245f7ad4c75339968"
    }
    Frame {
        msec: 2240
        hash: "b56e002e5eddde0245f7ad4c75339968"
    }
    Frame {
        msec: 2256
        hash: "b56e002e5eddde0245f7ad4c75339968"
    }
    Frame {
        msec: 2272
        hash: "b56e002e5eddde0245f7ad4c75339968"
    }
    Frame {
        msec: 2288
        hash: "b56e002e5eddde0245f7ad4c75339968"
    }
    Key {
        type: 6
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2304
        hash: "0bdd50e3c8b382b464c82d791ae6c1e5"
    }
    Key {
        type: 7
        key: 88
        modifiers: 0
        text: "78"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2320
        hash: "0bdd50e3c8b382b464c82d791ae6c1e5"
    }
    Frame {
        msec: 2336
        hash: "0bdd50e3c8b382b464c82d791ae6c1e5"
    }
    Frame {
        msec: 2352
        hash: "0bdd50e3c8b382b464c82d791ae6c1e5"
    }
    Key {
        type: 6
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2368
        hash: "b61b475b84c6e6a149f6262fc560b741"
    }
    Frame {
        msec: 2384
        hash: "b61b475b84c6e6a149f6262fc560b741"
    }
    Key {
        type: 7
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2400
        hash: "b61b475b84c6e6a149f6262fc560b741"
    }
    Frame {
        msec: 2416
        hash: "b61b475b84c6e6a149f6262fc560b741"
    }
    Frame {
        msec: 2432
        hash: "b61b475b84c6e6a149f6262fc560b741"
    }
    Key {
        type: 7
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2448
        hash: "b61b475b84c6e6a149f6262fc560b741"
    }
    Frame {
        msec: 2464
        hash: "b61b475b84c6e6a149f6262fc560b741"
    }
    Frame {
        msec: 2480
        hash: "b61b475b84c6e6a149f6262fc560b741"
    }
    Key {
        type: 6
        key: 69
        modifiers: 0
        text: "65"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2496
        hash: "1acd6152f317a6c8f6aca52ccf62a8c6"
    }
    Frame {
        msec: 2512
        hash: "1acd6152f317a6c8f6aca52ccf62a8c6"
    }
    Frame {
        msec: 2528
        hash: "1acd6152f317a6c8f6aca52ccf62a8c6"
    }
    Frame {
        msec: 2544
        hash: "1acd6152f317a6c8f6aca52ccf62a8c6"
    }
    Key {
        type: 7
        key: 69
        modifiers: 0
        text: "65"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2560
        hash: "1acd6152f317a6c8f6aca52ccf62a8c6"
    }
    Frame {
        msec: 2576
        hash: "1acd6152f317a6c8f6aca52ccf62a8c6"
    }
    Frame {
        msec: 2592
        hash: "1acd6152f317a6c8f6aca52ccf62a8c6"
    }
    Frame {
        msec: 2608
        hash: "1acd6152f317a6c8f6aca52ccf62a8c6"
    }
    Frame {
        msec: 2624
        hash: "1acd6152f317a6c8f6aca52ccf62a8c6"
    }
    Frame {
        msec: 2640
        hash: "1acd6152f317a6c8f6aca52ccf62a8c6"
    }
    Key {
        type: 6
        key: 68
        modifiers: 0
        text: "64"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2656
        hash: "90ab887de5fbf34f4d45e13c4b211490"
    }
    Frame {
        msec: 2672
        hash: "90ab887de5fbf34f4d45e13c4b211490"
    }
    Frame {
        msec: 2688
        hash: "90ab887de5fbf34f4d45e13c4b211490"
    }
    Frame {
        msec: 2704
        hash: "90ab887de5fbf34f4d45e13c4b211490"
    }
    Key {
        type: 7
        key: 68
        modifiers: 0
        text: "64"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2720
        hash: "90ab887de5fbf34f4d45e13c4b211490"
    }
    Frame {
        msec: 2736
        hash: "90ab887de5fbf34f4d45e13c4b211490"
    }
    Frame {
        msec: 2752
        hash: "90ab887de5fbf34f4d45e13c4b211490"
    }
    Key {
        type: 6
        key: 73
        modifiers: 0
        text: "69"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2768
        hash: "fc91281749bf1a844a19f20d87a17126"
    }
    Frame {
        msec: 2784
        hash: "fc91281749bf1a844a19f20d87a17126"
    }
    Frame {
        msec: 2800
        hash: "fc91281749bf1a844a19f20d87a17126"
    }
    Frame {
        msec: 2816
        hash: "fc91281749bf1a844a19f20d87a17126"
    }
    Frame {
        msec: 2832
        hash: "fc91281749bf1a844a19f20d87a17126"
    }
    Key {
        type: 6
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Key {
        type: 7
        key: 73
        modifiers: 0
        text: "69"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2848
        hash: "dcf6e510866fa20e54255c2c980d7b4b"
    }
    Frame {
        msec: 2864
        hash: "dcf6e510866fa20e54255c2c980d7b4b"
    }
    Frame {
        msec: 2880
        hash: "dcf6e510866fa20e54255c2c980d7b4b"
    }
    Frame {
        msec: 2896
        image: "wrap.3.png"
    }
    Key {
        type: 6
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2912
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Frame {
        msec: 2928
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Key {
        type: 7
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2944
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Frame {
        msec: 2960
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Key {
        type: 7
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2976
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Frame {
        msec: 2992
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Frame {
        msec: 3008
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Frame {
        msec: 3024
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Frame {
        msec: 3040
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Frame {
        msec: 3056
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Frame {
        msec: 3072
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Frame {
        msec: 3088
        hash: "a26b06714f951084f2ee5ee4b4e67e43"
    }
    Key {
        type: 6
        key: 87
        modifiers: 0
        text: "77"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3104
        hash: "832c43553cea6d22b7664ef6f145d1c6"
    }
    Frame {
        msec: 3120
        hash: "832c43553cea6d22b7664ef6f145d1c6"
    }
    Frame {
        msec: 3136
        hash: "832c43553cea6d22b7664ef6f145d1c6"
    }
    Frame {
        msec: 3152
        hash: "832c43553cea6d22b7664ef6f145d1c6"
    }
    Frame {
        msec: 3168
        hash: "832c43553cea6d22b7664ef6f145d1c6"
    }
    Key {
        type: 7
        key: 87
        modifiers: 0
        text: "77"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3184
        hash: "832c43553cea6d22b7664ef6f145d1c6"
    }
    Frame {
        msec: 3200
        hash: "832c43553cea6d22b7664ef6f145d1c6"
    }
    Frame {
        msec: 3216
        hash: "832c43553cea6d22b7664ef6f145d1c6"
    }
    Frame {
        msec: 3232
        hash: "832c43553cea6d22b7664ef6f145d1c6"
    }
    Frame {
        msec: 3248
        hash: "832c43553cea6d22b7664ef6f145d1c6"
    }
    Key {
        type: 6
        key: 82
        modifiers: 0
        text: "72"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3264
        hash: "081c183901aadcc6406f4ad9f41efa7e"
    }
    Frame {
        msec: 3280
        hash: "081c183901aadcc6406f4ad9f41efa7e"
    }
    Frame {
        msec: 3296
        hash: "081c183901aadcc6406f4ad9f41efa7e"
    }
    Key {
        type: 7
        key: 82
        modifiers: 0
        text: "72"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3312
        hash: "081c183901aadcc6406f4ad9f41efa7e"
    }
    Frame {
        msec: 3328
        hash: "081c183901aadcc6406f4ad9f41efa7e"
    }
    Frame {
        msec: 3344
        hash: "081c183901aadcc6406f4ad9f41efa7e"
    }
    Key {
        type: 6
        key: 65
        modifiers: 0
        text: "61"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3360
        hash: "9bd3c76a58f942880f40566cfbaa2e99"
    }
    Frame {
        msec: 3376
        hash: "9bd3c76a58f942880f40566cfbaa2e99"
    }
    Frame {
        msec: 3392
        hash: "9bd3c76a58f942880f40566cfbaa2e99"
    }
    Frame {
        msec: 3408
        hash: "9bd3c76a58f942880f40566cfbaa2e99"
    }
    Frame {
        msec: 3424
        hash: "9bd3c76a58f942880f40566cfbaa2e99"
    }
    Key {
        type: 7
        key: 65
        modifiers: 0
        text: "61"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3440
        hash: "9bd3c76a58f942880f40566cfbaa2e99"
    }
    Frame {
        msec: 3456
        hash: "9bd3c76a58f942880f40566cfbaa2e99"
    }
    Frame {
        msec: 3472
        hash: "9bd3c76a58f942880f40566cfbaa2e99"
    }
    Key {
        type: 6
        key: 80
        modifiers: 0
        text: "70"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3488
        hash: "204a2ee8a33e5452d47d95ad4142d417"
    }
    Frame {
        msec: 3504
        hash: "204a2ee8a33e5452d47d95ad4142d417"
    }
    Frame {
        msec: 3520
        hash: "204a2ee8a33e5452d47d95ad4142d417"
    }
    Frame {
        msec: 3536
        hash: "204a2ee8a33e5452d47d95ad4142d417"
    }
    Key {
        type: 7
        key: 80
        modifiers: 0
        text: "70"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3552
        hash: "204a2ee8a33e5452d47d95ad4142d417"
    }
    Frame {
        msec: 3568
        hash: "204a2ee8a33e5452d47d95ad4142d417"
    }
    Key {
        type: 6
        key: 83
        modifiers: 0
        text: "73"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3584
        hash: "4729d1f555fe604d4660f02673f9c5f3"
    }
    Frame {
        msec: 3600
        hash: "4729d1f555fe604d4660f02673f9c5f3"
    }
    Frame {
        msec: 3616
        hash: "4729d1f555fe604d4660f02673f9c5f3"
    }
    Frame {
        msec: 3632
        hash: "4729d1f555fe604d4660f02673f9c5f3"
    }
    Frame {
        msec: 3648
        hash: "4729d1f555fe604d4660f02673f9c5f3"
    }
    Frame {
        msec: 3664
        hash: "4729d1f555fe604d4660f02673f9c5f3"
    }
    Key {
        type: 6
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3680
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Key {
        type: 7
        key: 83
        modifiers: 0
        text: "73"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3696
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3712
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3728
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3744
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3760
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Key {
        type: 7
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3776
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3792
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3808
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3824
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3840
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3856
        image: "wrap.4.png"
    }
    Frame {
        msec: 3872
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3888
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Frame {
        msec: 3904
        hash: "2c0e0951ce4839b302a6e2735adc6c09"
    }
    Key {
        type: 6
        key: 67
        modifiers: 0
        text: "63"
        autorep: false
        count: 1
    }
    Frame {
        msec: 3920
        hash: "28c2ffe2ad35010dc077625cde7d21b6"
    }
    Frame {
        msec: 3936
        hash: "28c2ffe2ad35010dc077625cde7d21b6"
    }
    Frame {
        msec: 3952
        hash: "28c2ffe2ad35010dc077625cde7d21b6"
    }
    Frame {
        msec: 3968
        hash: "28c2ffe2ad35010dc077625cde7d21b6"
    }
    Frame {
        msec: 3984
        hash: "28c2ffe2ad35010dc077625cde7d21b6"
    }
    Key {
        type: 7
        key: 67
        modifiers: 0
        text: "63"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4000
        hash: "28c2ffe2ad35010dc077625cde7d21b6"
    }
    Frame {
        msec: 4016
        hash: "28c2ffe2ad35010dc077625cde7d21b6"
    }
    Key {
        type: 6
        key: 79
        modifiers: 0
        text: "6f"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4032
        hash: "6f206482adcd45a2b0d8d3c8b85f53c6"
    }
    Frame {
        msec: 4048
        hash: "6f206482adcd45a2b0d8d3c8b85f53c6"
    }
    Frame {
        msec: 4064
        hash: "6f206482adcd45a2b0d8d3c8b85f53c6"
    }
    Key {
        type: 7
        key: 79
        modifiers: 0
        text: "6f"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4080
        hash: "6f206482adcd45a2b0d8d3c8b85f53c6"
    }
    Key {
        type: 6
        key: 82
        modifiers: 0
        text: "72"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4096
        hash: "4685a786f36cb821a69b0ac059145a5f"
    }
    Frame {
        msec: 4112
        hash: "4685a786f36cb821a69b0ac059145a5f"
    }
    Frame {
        msec: 4128
        hash: "4685a786f36cb821a69b0ac059145a5f"
    }
    Frame {
        msec: 4144
        hash: "4685a786f36cb821a69b0ac059145a5f"
    }
    Key {
        type: 7
        key: 82
        modifiers: 0
        text: "72"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4160
        hash: "4685a786f36cb821a69b0ac059145a5f"
    }
    Frame {
        msec: 4176
        hash: "4685a786f36cb821a69b0ac059145a5f"
    }
    Frame {
        msec: 4192
        hash: "4685a786f36cb821a69b0ac059145a5f"
    }
    Key {
        type: 6
        key: 82
        modifiers: 0
        text: "72"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4208
        hash: "d0efb89ee3e2d2b18429b57dcfe13f33"
    }
    Frame {
        msec: 4224
        hash: "d0efb89ee3e2d2b18429b57dcfe13f33"
    }
    Frame {
        msec: 4240
        hash: "d0efb89ee3e2d2b18429b57dcfe13f33"
    }
    Frame {
        msec: 4256
        hash: "d0efb89ee3e2d2b18429b57dcfe13f33"
    }
    Frame {
        msec: 4272
        hash: "d0efb89ee3e2d2b18429b57dcfe13f33"
    }
    Key {
        type: 6
        key: 69
        modifiers: 0
        text: "65"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4288
        hash: "cbe0bb714b2e9b63af978f666292d8f0"
    }
    Key {
        type: 7
        key: 82
        modifiers: 0
        text: "72"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4304
        hash: "cbe0bb714b2e9b63af978f666292d8f0"
    }
    Frame {
        msec: 4320
        hash: "cbe0bb714b2e9b63af978f666292d8f0"
    }
    Frame {
        msec: 4336
        hash: "cbe0bb714b2e9b63af978f666292d8f0"
    }
    Key {
        type: 7
        key: 69
        modifiers: 0
        text: "65"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4352
        hash: "cbe0bb714b2e9b63af978f666292d8f0"
    }
    Frame {
        msec: 4368
        hash: "cbe0bb714b2e9b63af978f666292d8f0"
    }
    Frame {
        msec: 4384
        hash: "cbe0bb714b2e9b63af978f666292d8f0"
    }
    Frame {
        msec: 4400
        hash: "cbe0bb714b2e9b63af978f666292d8f0"
    }
    Frame {
        msec: 4416
        hash: "cbe0bb714b2e9b63af978f666292d8f0"
    }
    Key {
        type: 6
        key: 67
        modifiers: 0
        text: "63"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4432
        hash: "d15a45a86874daaff5f2e6afae43b2f4"
    }
    Frame {
        msec: 4448
        hash: "d15a45a86874daaff5f2e6afae43b2f4"
    }
    Frame {
        msec: 4464
        hash: "d15a45a86874daaff5f2e6afae43b2f4"
    }
    Key {
        type: 7
        key: 67
        modifiers: 0
        text: "63"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4480
        hash: "d15a45a86874daaff5f2e6afae43b2f4"
    }
    Frame {
        msec: 4496
        hash: "d15a45a86874daaff5f2e6afae43b2f4"
    }
    Frame {
        msec: 4512
        hash: "d15a45a86874daaff5f2e6afae43b2f4"
    }
    Frame {
        msec: 4528
        hash: "d15a45a86874daaff5f2e6afae43b2f4"
    }
    Frame {
        msec: 4544
        hash: "d15a45a86874daaff5f2e6afae43b2f4"
    }
    Key {
        type: 6
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4560
        hash: "b0c3ef9c5331af8768b23537d1d38311"
    }
    Frame {
        msec: 4576
        hash: "b0c3ef9c5331af8768b23537d1d38311"
    }
    Frame {
        msec: 4592
        hash: "b0c3ef9c5331af8768b23537d1d38311"
    }
    Frame {
        msec: 4608
        hash: "b0c3ef9c5331af8768b23537d1d38311"
    }
    Key {
        type: 7
        key: 84
        modifiers: 0
        text: "74"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4624
        hash: "b0c3ef9c5331af8768b23537d1d38311"
    }
    Frame {
        msec: 4640
        hash: "b0c3ef9c5331af8768b23537d1d38311"
    }
    Frame {
        msec: 4656
        hash: "b0c3ef9c5331af8768b23537d1d38311"
    }
    Frame {
        msec: 4672
        hash: "b0c3ef9c5331af8768b23537d1d38311"
    }
    Key {
        type: 6
        key: 76
        modifiers: 0
        text: "6c"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4688
        hash: "3be1d2faec1ab5d3d1ab72c25db95059"
    }
    Frame {
        msec: 4704
        hash: "3be1d2faec1ab5d3d1ab72c25db95059"
    }
    Frame {
        msec: 4720
        hash: "3be1d2faec1ab5d3d1ab72c25db95059"
    }
    Frame {
        msec: 4736
        hash: "3be1d2faec1ab5d3d1ab72c25db95059"
    }
    Key {
        type: 7
        key: 76
        modifiers: 0
        text: "6c"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4752
        hash: "3be1d2faec1ab5d3d1ab72c25db95059"
    }
    Frame {
        msec: 4768
        hash: "3be1d2faec1ab5d3d1ab72c25db95059"
    }
    Frame {
        msec: 4784
        hash: "3be1d2faec1ab5d3d1ab72c25db95059"
    }
    Key {
        type: 6
        key: 89
        modifiers: 0
        text: "79"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4800
        hash: "db999862fcf827930098b3f129ff567f"
    }
    Frame {
        msec: 4816
        image: "wrap.5.png"
    }
    Key {
        type: 7
        key: 89
        modifiers: 0
        text: "79"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4832
        hash: "db999862fcf827930098b3f129ff567f"
    }
    Frame {
        msec: 4848
        hash: "db999862fcf827930098b3f129ff567f"
    }
    Frame {
        msec: 4864
        hash: "db999862fcf827930098b3f129ff567f"
    }
    Frame {
        msec: 4880
        hash: "db999862fcf827930098b3f129ff567f"
    }
    Key {
        type: 6
        key: 46
        modifiers: 0
        text: "2e"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4896
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 4912
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 4928
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 4944
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Key {
        type: 7
        key: 46
        modifiers: 0
        text: "2e"
        autorep: false
        count: 1
    }
    Frame {
        msec: 4960
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 4976
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 4992
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5008
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5024
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5040
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5056
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5072
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5088
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5104
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5120
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5136
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5152
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5168
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5184
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5200
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5216
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5232
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5248
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5264
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5280
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5296
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5312
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5328
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5344
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5360
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5376
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5392
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5408
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5424
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5440
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5456
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5472
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5488
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5504
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5520
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5536
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5552
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5568
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5584
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5600
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5616
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5632
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5648
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5664
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5680
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5696
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5712
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5728
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5744
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5760
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5776
        image: "wrap.6.png"
    }
    Frame {
        msec: 5792
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5808
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5824
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5840
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5856
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5872
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5888
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5904
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5920
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5936
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5952
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5968
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 5984
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6000
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6016
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6032
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6048
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6064
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6080
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6096
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6112
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6128
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6144
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6160
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6176
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6192
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6208
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6224
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6240
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6256
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6272
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6288
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6304
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6320
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6336
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6352
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6368
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6384
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6400
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6416
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6432
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6448
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6464
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6480
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6496
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6512
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6528
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6544
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6560
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6576
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6592
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6608
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6624
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6640
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6656
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6672
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6688
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6704
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6720
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6736
        image: "wrap.7.png"
    }
    Frame {
        msec: 6752
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6768
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6784
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6800
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6816
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6832
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6848
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
    Frame {
        msec: 6864
        hash: "6557c4982e2c23d0ef5ec8a594df7277"
    }
}
