import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "qt-669.0.png"
    }
    Frame {
        msec: 32
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 48
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 64
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 80
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 96
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 112
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 128
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 144
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 160
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 176
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 192
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 208
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 224
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 240
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 256
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 272
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 288
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 304
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 320
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 336
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 352
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 368
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 384
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Key {
        type: 6
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 400
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 416
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 432
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Key {
        type: 7
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 448
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 464
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 480
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 496
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 512
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 528
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Key {
        type: 6
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 544
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 560
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 576
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Key {
        type: 7
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 592
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 608
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 624
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 640
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 656
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 672
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Key {
        type: 6
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 688
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 704
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 720
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 736
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Key {
        type: 7
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 752
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 768
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 784
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 800
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 816
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Key {
        type: 6
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 832
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 848
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 864
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 880
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Key {
        type: 7
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 896
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 912
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 928
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 944
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Key {
        type: 6
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 960
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 976
        image: "qt-669.1.png"
    }
    Frame {
        msec: 992
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 1008
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Key {
        type: 7
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1024
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 1040
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 1056
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 1072
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 1088
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Key {
        type: 6
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1104
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 1120
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 1136
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Key {
        type: 7
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1152
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 1168
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 1184
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 1200
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 1216
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Key {
        type: 6
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1232
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 1248
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 1264
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 1280
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Key {
        type: 7
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1296
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 1312
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 1328
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 1344
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Key {
        type: 6
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1360
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1376
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1392
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1408
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Key {
        type: 7
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1424
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1440
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1456
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1472
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1488
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Key {
        type: 6
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1504
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1520
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1536
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Key {
        type: 7
        key: 16777236
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1552
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1568
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1584
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1600
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1616
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1632
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1648
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1664
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1680
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1696
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1712
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1728
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1744
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1760
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1776
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1792
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1808
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1824
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Frame {
        msec: 1840
        hash: "64f5712c1f96345f2a2ad103e6fbd734"
    }
    Key {
        type: 6
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1856
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1872
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1888
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1904
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1920
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1936
        image: "qt-669.2.png"
    }
    Frame {
        msec: 1952
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1968
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 1984
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Key {
        type: 7
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2000
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 2016
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 2032
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 2048
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Frame {
        msec: 2064
        hash: "7f895d1255301397298cd6b92282e4f7"
    }
    Key {
        type: 6
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2080
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 2096
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 2112
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 2128
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 2144
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Key {
        type: 7
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2160
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 2176
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 2192
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 2208
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Frame {
        msec: 2224
        hash: "6ec9e863238467c249f62bdd38b68490"
    }
    Key {
        type: 6
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2240
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 2256
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Key {
        type: 7
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2272
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 2288
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 2304
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 2320
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Frame {
        msec: 2336
        hash: "fe998f3c7c780fddfa6a595936d2e78e"
    }
    Key {
        type: 6
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2352
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2368
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2384
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Key {
        type: 7
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2400
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2416
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2432
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2448
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2464
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2480
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2496
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2512
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2528
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2544
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2560
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2576
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Frame {
        msec: 2592
        hash: "a7415d0abcc670ba02c2a00b3b5fc647"
    }
    Key {
        type: 6
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2608
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 2624
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 2640
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 2656
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 2672
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Key {
        type: 7
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2688
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 2704
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 2720
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 2736
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Frame {
        msec: 2752
        hash: "9eda76efdd179847e89b9e96ead51e4a"
    }
    Key {
        type: 6
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2768
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 2784
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 2800
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Key {
        type: 7
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2816
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 2832
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 2848
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Frame {
        msec: 2864
        hash: "1bcb9bc9d6606329ad5376ea6f608bf8"
    }
    Key {
        type: 6
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2880
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 2896
        image: "qt-669.3.png"
    }
    Frame {
        msec: 2912
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 2928
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Key {
        type: 7
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 2944
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 2960
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 2976
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 2992
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Frame {
        msec: 3008
        hash: "1465f1f32ba4a6180ab3460298febe26"
    }
    Key {
        type: 6
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 3024
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3040
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3056
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3072
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3088
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Key {
        type: 7
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 3104
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3120
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3136
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3152
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3168
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3184
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3200
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3216
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3232
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3248
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3264
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3280
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3296
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3312
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3328
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3344
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3360
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3376
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3392
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3408
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3424
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3440
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3456
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3472
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3488
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3504
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3520
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3536
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3552
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3568
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3584
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3600
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3616
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3632
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3648
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3664
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3680
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Frame {
        msec: 3696
        hash: "89f3a1c5080d5d742e4455a8818a715c"
    }
    Key {
        type: 6
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 3712
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3728
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3744
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3760
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3776
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3792
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Key {
        type: 7
        key: 16777234
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 3808
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3824
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3840
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3856
        image: "qt-669.4.png"
    }
    Frame {
        msec: 3872
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3888
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3904
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3920
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3936
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3952
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3968
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 3984
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4000
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4016
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4032
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4048
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4064
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4080
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4096
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4112
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4128
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4144
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4160
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4176
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4192
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4208
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4224
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4240
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4256
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4272
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4288
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
    Frame {
        msec: 4304
        hash: "0051b27d72a917e2af72c4b953877d42"
    }
}
