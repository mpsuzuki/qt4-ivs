import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "echoMode.0.png"
    }
    Frame {
        msec: 32
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Key {
        type: 6
        key: 16777248
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 48
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 64
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 80
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 96
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 112
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 128
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 144
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 160
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 176
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 192
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 208
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 224
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 240
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 256
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 272
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 288
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 304
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 320
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 336
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Frame {
        msec: 352
        hash: "eff6a4491bc00e5570ea73a1371f63fc"
    }
    Key {
        type: 6
        key: 74
        modifiers: 33554432
        text: "4a"
        autorep: false
        count: 1
    }
    Frame {
        msec: 368
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 384
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 400
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 416
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 432
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Key {
        type: 7
        key: 74
        modifiers: 33554432
        text: "4a"
        autorep: false
        count: 1
    }
    Frame {
        msec: 448
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 464
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 480
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 496
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 512
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 528
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Key {
        type: 7
        key: 16777248
        modifiers: 33554432
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 544
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 560
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 576
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 592
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 608
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 624
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 640
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 656
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 672
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Frame {
        msec: 688
        hash: "00097f2bb5cf4ea412db48acb93ffd76"
    }
    Key {
        type: 6
        key: 65
        modifiers: 0
        text: "61"
        autorep: false
        count: 1
    }
    Frame {
        msec: 704
        hash: "94e683223900efc840296b86ce934ec3"
    }
    Frame {
        msec: 720
        hash: "94e683223900efc840296b86ce934ec3"
    }
    Frame {
        msec: 736
        hash: "94e683223900efc840296b86ce934ec3"
    }
    Frame {
        msec: 752
        hash: "94e683223900efc840296b86ce934ec3"
    }
    Frame {
        msec: 768
        hash: "94e683223900efc840296b86ce934ec3"
    }
    Key {
        type: 7
        key: 65
        modifiers: 0
        text: "61"
        autorep: false
        count: 1
    }
    Frame {
        msec: 784
        hash: "94e683223900efc840296b86ce934ec3"
    }
    Frame {
        msec: 800
        hash: "94e683223900efc840296b86ce934ec3"
    }
    Frame {
        msec: 816
        hash: "94e683223900efc840296b86ce934ec3"
    }
    Frame {
        msec: 832
        hash: "94e683223900efc840296b86ce934ec3"
    }
    Frame {
        msec: 848
        hash: "94e683223900efc840296b86ce934ec3"
    }
    Key {
        type: 6
        key: 67
        modifiers: 0
        text: "63"
        autorep: false
        count: 1
    }
    Frame {
        msec: 864
        hash: "a1c7aeece2891f3ca0103761ffa7f424"
    }
    Frame {
        msec: 880
        hash: "a1c7aeece2891f3ca0103761ffa7f424"
    }
    Frame {
        msec: 896
        hash: "a1c7aeece2891f3ca0103761ffa7f424"
    }
    Key {
        type: 7
        key: 67
        modifiers: 0
        text: "63"
        autorep: false
        count: 1
    }
    Frame {
        msec: 912
        hash: "a1c7aeece2891f3ca0103761ffa7f424"
    }
    Frame {
        msec: 928
        hash: "a1c7aeece2891f3ca0103761ffa7f424"
    }
    Frame {
        msec: 944
        hash: "a1c7aeece2891f3ca0103761ffa7f424"
    }
    Frame {
        msec: 960
        hash: "a1c7aeece2891f3ca0103761ffa7f424"
    }
    Frame {
        msec: 976
        image: "echoMode.1.png"
    }
    Key {
        type: 6
        key: 75
        modifiers: 0
        text: "6b"
        autorep: false
        count: 1
    }
    Frame {
        msec: 992
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1008
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1024
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1040
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Key {
        type: 7
        key: 75
        modifiers: 0
        text: "6b"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1056
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1072
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1088
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1104
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1120
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1136
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1152
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1168
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1184
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1200
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1216
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Frame {
        msec: 1232
        hash: "7a4ebe5f0875ded07b44c9ff2d6a4d75"
    }
    Key {
        type: 6
        key: 68
        modifiers: 0
        text: "64"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1248
        hash: "b7cdd294253e065c06fabc60895a29c2"
    }
    Frame {
        msec: 1264
        hash: "b7cdd294253e065c06fabc60895a29c2"
    }
    Frame {
        msec: 1280
        hash: "b7cdd294253e065c06fabc60895a29c2"
    }
    Key {
        type: 7
        key: 68
        modifiers: 0
        text: "64"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1296
        hash: "b7cdd294253e065c06fabc60895a29c2"
    }
    Frame {
        msec: 1312
        hash: "b7cdd294253e065c06fabc60895a29c2"
    }
    Frame {
        msec: 1328
        hash: "b7cdd294253e065c06fabc60895a29c2"
    }
    Key {
        type: 6
        key: 65
        modifiers: 0
        text: "61"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1344
        hash: "d8669a3194f485aaef3a1421f7fd50f6"
    }
    Frame {
        msec: 1360
        hash: "d8669a3194f485aaef3a1421f7fd50f6"
    }
    Frame {
        msec: 1376
        hash: "d8669a3194f485aaef3a1421f7fd50f6"
    }
    Frame {
        msec: 1392
        hash: "d8669a3194f485aaef3a1421f7fd50f6"
    }
    Frame {
        msec: 1408
        hash: "d8669a3194f485aaef3a1421f7fd50f6"
    }
    Frame {
        msec: 1424
        hash: "d8669a3194f485aaef3a1421f7fd50f6"
    }
    Frame {
        msec: 1440
        hash: "d8669a3194f485aaef3a1421f7fd50f6"
    }
    Frame {
        msec: 1456
        hash: "d8669a3194f485aaef3a1421f7fd50f6"
    }
    Frame {
        msec: 1472
        hash: "d8669a3194f485aaef3a1421f7fd50f6"
    }
    Key {
        type: 7
        key: 65
        modifiers: 0
        text: "61"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1488
        hash: "d8669a3194f485aaef3a1421f7fd50f6"
    }
    Key {
        type: 6
        key: 87
        modifiers: 0
        text: "77"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1504
        hash: "b53fd36f58dc692856e6a789371aaf33"
    }
    Frame {
        msec: 1520
        hash: "b53fd36f58dc692856e6a789371aaf33"
    }
    Frame {
        msec: 1536
        hash: "b53fd36f58dc692856e6a789371aaf33"
    }
    Frame {
        msec: 1552
        hash: "b53fd36f58dc692856e6a789371aaf33"
    }
    Key {
        type: 7
        key: 87
        modifiers: 0
        text: "77"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1568
        hash: "b53fd36f58dc692856e6a789371aaf33"
    }
    Frame {
        msec: 1584
        hash: "b53fd36f58dc692856e6a789371aaf33"
    }
    Frame {
        msec: 1600
        hash: "b53fd36f58dc692856e6a789371aaf33"
    }
    Frame {
        msec: 1616
        hash: "b53fd36f58dc692856e6a789371aaf33"
    }
    Frame {
        msec: 1632
        hash: "b53fd36f58dc692856e6a789371aaf33"
    }
    Frame {
        msec: 1648
        hash: "b53fd36f58dc692856e6a789371aaf33"
    }
    Key {
        type: 6
        key: 83
        modifiers: 0
        text: "73"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1664
        hash: "98de66666f6ea1a87bd493db3f67a7c6"
    }
    Frame {
        msec: 1680
        hash: "98de66666f6ea1a87bd493db3f67a7c6"
    }
    Frame {
        msec: 1696
        hash: "98de66666f6ea1a87bd493db3f67a7c6"
    }
    Frame {
        msec: 1712
        hash: "98de66666f6ea1a87bd493db3f67a7c6"
    }
    Frame {
        msec: 1728
        hash: "98de66666f6ea1a87bd493db3f67a7c6"
    }
    Key {
        type: 6
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1744
        hash: "696807419ef2b228dfb9d85dd79dd293"
    }
    Key {
        type: 7
        key: 83
        modifiers: 0
        text: "73"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1760
        hash: "696807419ef2b228dfb9d85dd79dd293"
    }
    Frame {
        msec: 1776
        hash: "696807419ef2b228dfb9d85dd79dd293"
    }
    Frame {
        msec: 1792
        hash: "696807419ef2b228dfb9d85dd79dd293"
    }
    Key {
        type: 7
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1808
        hash: "696807419ef2b228dfb9d85dd79dd293"
    }
    Frame {
        msec: 1824
        hash: "696807419ef2b228dfb9d85dd79dd293"
    }
    Frame {
        msec: 1840
        hash: "696807419ef2b228dfb9d85dd79dd293"
    }
    Frame {
        msec: 1856
        hash: "696807419ef2b228dfb9d85dd79dd293"
    }
    Key {
        type: 6
        key: 76
        modifiers: 0
        text: "6c"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1872
        hash: "4c0a528609872cf65180d336bbca4231"
    }
    Frame {
        msec: 1888
        hash: "4c0a528609872cf65180d336bbca4231"
    }
    Frame {
        msec: 1904
        hash: "4c0a528609872cf65180d336bbca4231"
    }
    Frame {
        msec: 1920
        hash: "4c0a528609872cf65180d336bbca4231"
    }
    Key {
        type: 7
        key: 76
        modifiers: 0
        text: "6c"
        autorep: false
        count: 1
    }
    Frame {
        msec: 1936
        image: "echoMode.2.png"
    }
    Frame {
        msec: 1952
        hash: "4c0a528609872cf65180d336bbca4231"
    }
    Frame {
        msec: 1968
        hash: "4c0a528609872cf65180d336bbca4231"
    }
    Frame {
        msec: 1984
        hash: "4c0a528609872cf65180d336bbca4231"
    }
    Frame {
        msec: 2000
        hash: "4c0a528609872cf65180d336bbca4231"
    }
    Frame {
        msec: 2016
        hash: "4c0a528609872cf65180d336bbca4231"
    }
    Key {
        type: 6
        key: 79
        modifiers: 0
        text: "6f"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2032
        hash: "03b670f413abfa1811d4020de969b2ea"
    }
    Frame {
        msec: 2048
        hash: "03b670f413abfa1811d4020de969b2ea"
    }
    Key {
        type: 7
        key: 79
        modifiers: 0
        text: "6f"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2064
        hash: "03b670f413abfa1811d4020de969b2ea"
    }
    Frame {
        msec: 2080
        hash: "03b670f413abfa1811d4020de969b2ea"
    }
    Key {
        type: 6
        key: 86
        modifiers: 0
        text: "76"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2096
        hash: "6d478c62fa5bb37f0178e94914473174"
    }
    Frame {
        msec: 2112
        hash: "6d478c62fa5bb37f0178e94914473174"
    }
    Frame {
        msec: 2128
        hash: "6d478c62fa5bb37f0178e94914473174"
    }
    Frame {
        msec: 2144
        hash: "6d478c62fa5bb37f0178e94914473174"
    }
    Key {
        type: 6
        key: 69
        modifiers: 0
        text: "65"
        autorep: false
        count: 1
    }
    Key {
        type: 7
        key: 86
        modifiers: 0
        text: "76"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2160
        hash: "2f9803e906ce38a6ade3874bbeb27216"
    }
    Frame {
        msec: 2176
        hash: "2f9803e906ce38a6ade3874bbeb27216"
    }
    Frame {
        msec: 2192
        hash: "2f9803e906ce38a6ade3874bbeb27216"
    }
    Frame {
        msec: 2208
        hash: "2f9803e906ce38a6ade3874bbeb27216"
    }
    Key {
        type: 6
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2224
        hash: "d93582b0c7de46d5ff1c9959c158bfe7"
    }
    Key {
        type: 7
        key: 69
        modifiers: 0
        text: "65"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2240
        hash: "d93582b0c7de46d5ff1c9959c158bfe7"
    }
    Frame {
        msec: 2256
        hash: "d93582b0c7de46d5ff1c9959c158bfe7"
    }
    Frame {
        msec: 2272
        hash: "d93582b0c7de46d5ff1c9959c158bfe7"
    }
    Frame {
        msec: 2288
        hash: "d93582b0c7de46d5ff1c9959c158bfe7"
    }
    Frame {
        msec: 2304
        hash: "d93582b0c7de46d5ff1c9959c158bfe7"
    }
    Key {
        type: 7
        key: 32
        modifiers: 0
        text: "20"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2320
        hash: "d93582b0c7de46d5ff1c9959c158bfe7"
    }
    Frame {
        msec: 2336
        hash: "d93582b0c7de46d5ff1c9959c158bfe7"
    }
    Key {
        type: 6
        key: 77
        modifiers: 0
        text: "6d"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2352
        hash: "8accfa30ddc59803d8f9d2f60dd6a891"
    }
    Frame {
        msec: 2368
        hash: "8accfa30ddc59803d8f9d2f60dd6a891"
    }
    Frame {
        msec: 2384
        hash: "8accfa30ddc59803d8f9d2f60dd6a891"
    }
    Frame {
        msec: 2400
        hash: "8accfa30ddc59803d8f9d2f60dd6a891"
    }
    Frame {
        msec: 2416
        hash: "8accfa30ddc59803d8f9d2f60dd6a891"
    }
    Frame {
        msec: 2432
        hash: "8accfa30ddc59803d8f9d2f60dd6a891"
    }
    Key {
        type: 7
        key: 77
        modifiers: 0
        text: "6d"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2448
        hash: "8accfa30ddc59803d8f9d2f60dd6a891"
    }
    Frame {
        msec: 2464
        hash: "8accfa30ddc59803d8f9d2f60dd6a891"
    }
    Frame {
        msec: 2480
        hash: "8accfa30ddc59803d8f9d2f60dd6a891"
    }
    Frame {
        msec: 2496
        hash: "8accfa30ddc59803d8f9d2f60dd6a891"
    }
    Key {
        type: 6
        key: 89
        modifiers: 0
        text: "79"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2512
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2528
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2544
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Key {
        type: 7
        key: 89
        modifiers: 0
        text: "79"
        autorep: false
        count: 1
    }
    Frame {
        msec: 2560
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2576
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2592
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2608
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2624
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2640
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2656
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2672
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2688
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2704
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2720
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2736
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2752
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2768
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2784
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2800
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2816
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2832
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2848
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2864
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2880
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2896
        image: "echoMode.3.png"
    }
    Frame {
        msec: 2912
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2928
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2944
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2960
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2976
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 2992
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 3008
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 3024
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 3040
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
    Frame {
        msec: 3056
        hash: "a444ce402f5dc0d892f66a88b8252301"
    }
}
