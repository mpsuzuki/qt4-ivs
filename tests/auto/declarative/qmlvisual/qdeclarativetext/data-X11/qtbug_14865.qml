import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "qtbug_14865.0.png"
    }
    Frame {
        msec: 32
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 48
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 64
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 80
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 96
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 112
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 128
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 144
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 160
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 176
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 192
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 208
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 224
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 240
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 256
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 272
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 288
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 304
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 320
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 336
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 352
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 368
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 384
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 400
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 416
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 432
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 448
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 464
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 480
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 496
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 512
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 528
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 544
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 560
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 576
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 592
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 608
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 624
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 640
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 656
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 672
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 688
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 704
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 720
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 736
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 752
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 768
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 784
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 800
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 816
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 832
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 848
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 864
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 880
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 896
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 912
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 928
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 944
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 960
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 976
        image: "qtbug_14865.1.png"
    }
    Frame {
        msec: 992
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 1008
        hash: "4235bd6abcbdf6621c4c41153fbaada5"
    }
    Frame {
        msec: 1024
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1040
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1056
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1072
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1088
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1104
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1120
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1136
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1152
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1168
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1184
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1200
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1216
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1232
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1248
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1264
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1280
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1296
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1312
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1328
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1344
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1360
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1376
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1392
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1408
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1424
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1440
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1456
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1472
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1488
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1504
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1520
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1536
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1552
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1568
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1584
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1600
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1616
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1632
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1648
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1664
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1680
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1696
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1712
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1728
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1744
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
    Frame {
        msec: 1760
        hash: "3ccd3d26158a50d8f0567bafd7a23e06"
    }
}
