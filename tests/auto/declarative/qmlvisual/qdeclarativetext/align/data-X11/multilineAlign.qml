import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "multilineAlign.0.png"
    }
    Frame {
        msec: 32
        hash: "d80fd046c582a26230e547471f290f12"
    }
    Frame {
        msec: 48
        hash: "d80fd046c582a26230e547471f290f12"
    }
    Frame {
        msec: 64
        hash: "d80fd046c582a26230e547471f290f12"
    }
    Frame {
        msec: 80
        hash: "d80fd046c582a26230e547471f290f12"
    }
    Frame {
        msec: 96
        hash: "d80fd046c582a26230e547471f290f12"
    }
    Frame {
        msec: 112
        hash: "d80fd046c582a26230e547471f290f12"
    }
    Frame {
        msec: 128
        hash: "d80fd046c582a26230e547471f290f12"
    }
    Frame {
        msec: 144
        hash: "d80fd046c582a26230e547471f290f12"
    }
    Frame {
        msec: 160
        hash: "d80fd046c582a26230e547471f290f12"
    }
    Frame {
        msec: 176
        hash: "f9e466557e920150c638621536d94e5b"
    }
    Frame {
        msec: 192
        hash: "f9e466557e920150c638621536d94e5b"
    }
    Frame {
        msec: 208
        hash: "f9e466557e920150c638621536d94e5b"
    }
    Frame {
        msec: 224
        hash: "f9e466557e920150c638621536d94e5b"
    }
    Frame {
        msec: 240
        hash: "f9e466557e920150c638621536d94e5b"
    }
    Frame {
        msec: 256
        hash: "f9e466557e920150c638621536d94e5b"
    }
    Frame {
        msec: 272
        hash: "f9e466557e920150c638621536d94e5b"
    }
    Frame {
        msec: 288
        hash: "f9e466557e920150c638621536d94e5b"
    }
    Frame {
        msec: 304
        hash: "f9e466557e920150c638621536d94e5b"
    }
    Frame {
        msec: 320
        hash: "f9e466557e920150c638621536d94e5b"
    }
    Frame {
        msec: 336
        hash: "40b5718a9370c332f254a3ead05dfe5b"
    }
    Frame {
        msec: 352
        hash: "40b5718a9370c332f254a3ead05dfe5b"
    }
    Frame {
        msec: 368
        hash: "40b5718a9370c332f254a3ead05dfe5b"
    }
    Frame {
        msec: 384
        hash: "40b5718a9370c332f254a3ead05dfe5b"
    }
    Frame {
        msec: 400
        hash: "40b5718a9370c332f254a3ead05dfe5b"
    }
    Frame {
        msec: 416
        hash: "40b5718a9370c332f254a3ead05dfe5b"
    }
    Frame {
        msec: 432
        hash: "40b5718a9370c332f254a3ead05dfe5b"
    }
    Frame {
        msec: 448
        hash: "40b5718a9370c332f254a3ead05dfe5b"
    }
    Frame {
        msec: 464
        hash: "40b5718a9370c332f254a3ead05dfe5b"
    }
    Frame {
        msec: 480
        hash: "40b5718a9370c332f254a3ead05dfe5b"
    }
    Frame {
        msec: 496
        hash: "3249c560c69e915020f9632acd1c5eca"
    }
    Frame {
        msec: 512
        hash: "3249c560c69e915020f9632acd1c5eca"
    }
    Frame {
        msec: 528
        hash: "3249c560c69e915020f9632acd1c5eca"
    }
    Frame {
        msec: 544
        hash: "3249c560c69e915020f9632acd1c5eca"
    }
    Frame {
        msec: 560
        hash: "3249c560c69e915020f9632acd1c5eca"
    }
    Frame {
        msec: 576
        hash: "3249c560c69e915020f9632acd1c5eca"
    }
    Frame {
        msec: 592
        hash: "3249c560c69e915020f9632acd1c5eca"
    }
    Frame {
        msec: 608
        hash: "3249c560c69e915020f9632acd1c5eca"
    }
    Frame {
        msec: 624
        hash: "3249c560c69e915020f9632acd1c5eca"
    }
    Frame {
        msec: 640
        hash: "3249c560c69e915020f9632acd1c5eca"
    }
    Frame {
        msec: 656
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 672
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 688
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 704
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 720
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 736
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 752
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 768
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 784
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 800
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 816
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 832
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 848
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 864
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 880
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 896
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 912
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 928
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 944
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
    Frame {
        msec: 960
        hash: "2df61c56ba08ef258a0d493760127a8d"
    }
}
