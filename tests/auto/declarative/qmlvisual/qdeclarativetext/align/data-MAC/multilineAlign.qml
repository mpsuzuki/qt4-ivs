import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "multilineAlign.0.png"
    }
    Frame {
        msec: 32
        hash: "7fb2062f5786da9323db4286688682a0"
    }
    Frame {
        msec: 48
        hash: "7fb2062f5786da9323db4286688682a0"
    }
    Frame {
        msec: 64
        hash: "7fb2062f5786da9323db4286688682a0"
    }
    Frame {
        msec: 80
        hash: "7fb2062f5786da9323db4286688682a0"
    }
    Frame {
        msec: 96
        hash: "7fb2062f5786da9323db4286688682a0"
    }
    Frame {
        msec: 112
        hash: "7fb2062f5786da9323db4286688682a0"
    }
    Frame {
        msec: 128
        hash: "7fb2062f5786da9323db4286688682a0"
    }
    Frame {
        msec: 144
        hash: "7fb2062f5786da9323db4286688682a0"
    }
    Frame {
        msec: 160
        hash: "7fb2062f5786da9323db4286688682a0"
    }
    Frame {
        msec: 176
        hash: "c67a5ae840827487ab618ff2d4e9a056"
    }
    Frame {
        msec: 192
        hash: "c67a5ae840827487ab618ff2d4e9a056"
    }
    Frame {
        msec: 208
        hash: "c67a5ae840827487ab618ff2d4e9a056"
    }
    Frame {
        msec: 224
        hash: "c67a5ae840827487ab618ff2d4e9a056"
    }
    Frame {
        msec: 240
        hash: "c67a5ae840827487ab618ff2d4e9a056"
    }
    Frame {
        msec: 256
        hash: "c67a5ae840827487ab618ff2d4e9a056"
    }
    Frame {
        msec: 272
        hash: "c67a5ae840827487ab618ff2d4e9a056"
    }
    Frame {
        msec: 288
        hash: "c67a5ae840827487ab618ff2d4e9a056"
    }
    Frame {
        msec: 304
        hash: "c67a5ae840827487ab618ff2d4e9a056"
    }
    Frame {
        msec: 320
        hash: "c67a5ae840827487ab618ff2d4e9a056"
    }
    Frame {
        msec: 336
        hash: "c7986aca05835e238ee95be063bdd032"
    }
    Frame {
        msec: 352
        hash: "c7986aca05835e238ee95be063bdd032"
    }
    Frame {
        msec: 368
        hash: "c7986aca05835e238ee95be063bdd032"
    }
    Frame {
        msec: 384
        hash: "c7986aca05835e238ee95be063bdd032"
    }
    Frame {
        msec: 400
        hash: "c7986aca05835e238ee95be063bdd032"
    }
    Frame {
        msec: 416
        hash: "c7986aca05835e238ee95be063bdd032"
    }
    Frame {
        msec: 432
        hash: "c7986aca05835e238ee95be063bdd032"
    }
    Frame {
        msec: 448
        hash: "c7986aca05835e238ee95be063bdd032"
    }
    Frame {
        msec: 464
        hash: "c7986aca05835e238ee95be063bdd032"
    }
    Frame {
        msec: 480
        hash: "c7986aca05835e238ee95be063bdd032"
    }
    Frame {
        msec: 496
        hash: "dd8ee9c060450beef6cc2494fa463e0a"
    }
    Frame {
        msec: 512
        hash: "dd8ee9c060450beef6cc2494fa463e0a"
    }
    Frame {
        msec: 528
        hash: "dd8ee9c060450beef6cc2494fa463e0a"
    }
    Frame {
        msec: 544
        hash: "dd8ee9c060450beef6cc2494fa463e0a"
    }
    Frame {
        msec: 560
        hash: "dd8ee9c060450beef6cc2494fa463e0a"
    }
    Frame {
        msec: 576
        hash: "dd8ee9c060450beef6cc2494fa463e0a"
    }
    Frame {
        msec: 592
        hash: "dd8ee9c060450beef6cc2494fa463e0a"
    }
    Frame {
        msec: 608
        hash: "dd8ee9c060450beef6cc2494fa463e0a"
    }
    Frame {
        msec: 624
        hash: "dd8ee9c060450beef6cc2494fa463e0a"
    }
    Frame {
        msec: 640
        hash: "dd8ee9c060450beef6cc2494fa463e0a"
    }
    Frame {
        msec: 656
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 672
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 688
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 704
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 720
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 736
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 752
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 768
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 784
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 800
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 816
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 832
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 848
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 864
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 880
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 896
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 912
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 928
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 944
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
    Frame {
        msec: 960
        hash: "f55ebe08f1b538d085cda157f566859e"
    }
}
