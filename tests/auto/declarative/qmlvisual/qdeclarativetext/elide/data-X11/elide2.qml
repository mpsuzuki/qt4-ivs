import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "elide2.0.png"
    }
    Frame {
        msec: 32
        hash: "0922fd48af050774d53e0b3815d57d8e"
    }
    Frame {
        msec: 48
        hash: "0922fd48af050774d53e0b3815d57d8e"
    }
    Frame {
        msec: 64
        hash: "0922fd48af050774d53e0b3815d57d8e"
    }
    Frame {
        msec: 80
        hash: "0922fd48af050774d53e0b3815d57d8e"
    }
    Frame {
        msec: 96
        hash: "0922fd48af050774d53e0b3815d57d8e"
    }
    Frame {
        msec: 112
        hash: "0922fd48af050774d53e0b3815d57d8e"
    }
    Frame {
        msec: 128
        hash: "6ed734d7092a34e440628dc70db97ac5"
    }
    Frame {
        msec: 144
        hash: "6ed734d7092a34e440628dc70db97ac5"
    }
    Frame {
        msec: 160
        hash: "6ed734d7092a34e440628dc70db97ac5"
    }
    Frame {
        msec: 176
        hash: "6ed734d7092a34e440628dc70db97ac5"
    }
    Frame {
        msec: 192
        hash: "a74b735016141dccf2c84fe9ee1e3ab2"
    }
    Frame {
        msec: 208
        hash: "a74b735016141dccf2c84fe9ee1e3ab2"
    }
    Frame {
        msec: 224
        hash: "a74b735016141dccf2c84fe9ee1e3ab2"
    }
    Frame {
        msec: 240
        hash: "a74b735016141dccf2c84fe9ee1e3ab2"
    }
    Frame {
        msec: 256
        hash: "047416b9368fb352b7da1e073d863e96"
    }
    Frame {
        msec: 272
        hash: "047416b9368fb352b7da1e073d863e96"
    }
    Frame {
        msec: 288
        hash: "047416b9368fb352b7da1e073d863e96"
    }
    Frame {
        msec: 304
        hash: "047416b9368fb352b7da1e073d863e96"
    }
    Frame {
        msec: 320
        hash: "f2d62e8675b8bba924b27db689c9cd7f"
    }
    Frame {
        msec: 336
        hash: "f2d62e8675b8bba924b27db689c9cd7f"
    }
    Frame {
        msec: 352
        hash: "f2d62e8675b8bba924b27db689c9cd7f"
    }
    Frame {
        msec: 368
        hash: "f2d62e8675b8bba924b27db689c9cd7f"
    }
    Frame {
        msec: 384
        hash: "f2d62e8675b8bba924b27db689c9cd7f"
    }
    Frame {
        msec: 400
        hash: "9498a80d60ab24d82ffb935979e1cf1b"
    }
    Frame {
        msec: 416
        hash: "9498a80d60ab24d82ffb935979e1cf1b"
    }
    Frame {
        msec: 432
        hash: "9498a80d60ab24d82ffb935979e1cf1b"
    }
    Frame {
        msec: 448
        hash: "9498a80d60ab24d82ffb935979e1cf1b"
    }
    Frame {
        msec: 464
        hash: "ee3cb45a15460f4235fc22ca97e0303d"
    }
    Frame {
        msec: 480
        hash: "ee3cb45a15460f4235fc22ca97e0303d"
    }
    Frame {
        msec: 496
        hash: "ee3cb45a15460f4235fc22ca97e0303d"
    }
    Frame {
        msec: 512
        hash: "ee3cb45a15460f4235fc22ca97e0303d"
    }
    Frame {
        msec: 528
        hash: "94464db418aec12b451e9dc106deec73"
    }
    Frame {
        msec: 544
        hash: "94464db418aec12b451e9dc106deec73"
    }
    Frame {
        msec: 560
        hash: "94464db418aec12b451e9dc106deec73"
    }
    Frame {
        msec: 576
        hash: "94464db418aec12b451e9dc106deec73"
    }
    Frame {
        msec: 592
        hash: "94464db418aec12b451e9dc106deec73"
    }
    Frame {
        msec: 608
        hash: "22b23a55986e912cf38239d5e68f0c4a"
    }
    Frame {
        msec: 624
        hash: "22b23a55986e912cf38239d5e68f0c4a"
    }
    Frame {
        msec: 640
        hash: "22b23a55986e912cf38239d5e68f0c4a"
    }
    Frame {
        msec: 656
        hash: "22b23a55986e912cf38239d5e68f0c4a"
    }
    Frame {
        msec: 672
        hash: "3836d0aaf354d147dc6ffe3ace184ba5"
    }
    Frame {
        msec: 688
        hash: "3836d0aaf354d147dc6ffe3ace184ba5"
    }
    Frame {
        msec: 704
        hash: "3836d0aaf354d147dc6ffe3ace184ba5"
    }
    Frame {
        msec: 720
        hash: "3836d0aaf354d147dc6ffe3ace184ba5"
    }
    Frame {
        msec: 736
        hash: "3836d0aaf354d147dc6ffe3ace184ba5"
    }
    Frame {
        msec: 752
        hash: "20ccea5bc4c15401a7c660b1801488dd"
    }
    Frame {
        msec: 768
        hash: "20ccea5bc4c15401a7c660b1801488dd"
    }
    Frame {
        msec: 784
        hash: "20ccea5bc4c15401a7c660b1801488dd"
    }
    Frame {
        msec: 800
        hash: "20ccea5bc4c15401a7c660b1801488dd"
    }
    Frame {
        msec: 816
        hash: "31ffa9cfd6f60a33ed3b052e45ee5080"
    }
    Frame {
        msec: 832
        hash: "31ffa9cfd6f60a33ed3b052e45ee5080"
    }
    Frame {
        msec: 848
        hash: "31ffa9cfd6f60a33ed3b052e45ee5080"
    }
    Frame {
        msec: 864
        hash: "31ffa9cfd6f60a33ed3b052e45ee5080"
    }
    Frame {
        msec: 880
        hash: "7138b38fcff27e85aaf3179c6e81ac69"
    }
    Frame {
        msec: 896
        hash: "7138b38fcff27e85aaf3179c6e81ac69"
    }
    Frame {
        msec: 912
        hash: "7138b38fcff27e85aaf3179c6e81ac69"
    }
    Frame {
        msec: 928
        hash: "7138b38fcff27e85aaf3179c6e81ac69"
    }
    Frame {
        msec: 944
        hash: "7138b38fcff27e85aaf3179c6e81ac69"
    }
    Frame {
        msec: 960
        hash: "78854022288d4cd50bb9141896403d35"
    }
    Frame {
        msec: 976
        image: "elide2.1.png"
    }
    Frame {
        msec: 992
        hash: "78854022288d4cd50bb9141896403d35"
    }
    Frame {
        msec: 1008
        hash: "78854022288d4cd50bb9141896403d35"
    }
    Frame {
        msec: 1024
        hash: "8730d8adb4029b157e39b90e3cb2b879"
    }
    Frame {
        msec: 1040
        hash: "8730d8adb4029b157e39b90e3cb2b879"
    }
    Frame {
        msec: 1056
        hash: "8730d8adb4029b157e39b90e3cb2b879"
    }
    Frame {
        msec: 1072
        hash: "8730d8adb4029b157e39b90e3cb2b879"
    }
    Frame {
        msec: 1088
        hash: "9edb542976d1acd86be3d516276dee1f"
    }
    Frame {
        msec: 1104
        hash: "9edb542976d1acd86be3d516276dee1f"
    }
    Frame {
        msec: 1120
        hash: "9edb542976d1acd86be3d516276dee1f"
    }
    Frame {
        msec: 1136
        hash: "9edb542976d1acd86be3d516276dee1f"
    }
    Frame {
        msec: 1152
        hash: "9edb542976d1acd86be3d516276dee1f"
    }
    Frame {
        msec: 1168
        hash: "1a394542b01712fbd67b78a69733b324"
    }
    Frame {
        msec: 1184
        hash: "1a394542b01712fbd67b78a69733b324"
    }
    Frame {
        msec: 1200
        hash: "1a394542b01712fbd67b78a69733b324"
    }
    Frame {
        msec: 1216
        hash: "1a394542b01712fbd67b78a69733b324"
    }
    Frame {
        msec: 1232
        hash: "4825f9a6679fdee8efe89507d384c07c"
    }
    Frame {
        msec: 1248
        hash: "4825f9a6679fdee8efe89507d384c07c"
    }
    Frame {
        msec: 1264
        hash: "4825f9a6679fdee8efe89507d384c07c"
    }
    Frame {
        msec: 1280
        hash: "4825f9a6679fdee8efe89507d384c07c"
    }
    Frame {
        msec: 1296
        hash: "4825f9a6679fdee8efe89507d384c07c"
    }
    Frame {
        msec: 1312
        hash: "0ed5382fd2e370bad934647d7abf293f"
    }
    Frame {
        msec: 1328
        hash: "0ed5382fd2e370bad934647d7abf293f"
    }
    Frame {
        msec: 1344
        hash: "0ed5382fd2e370bad934647d7abf293f"
    }
    Frame {
        msec: 1360
        hash: "0ed5382fd2e370bad934647d7abf293f"
    }
    Frame {
        msec: 1376
        hash: "6206435ab4d05d5d5f84b362d45c30f9"
    }
    Frame {
        msec: 1392
        hash: "6206435ab4d05d5d5f84b362d45c30f9"
    }
    Frame {
        msec: 1408
        hash: "6206435ab4d05d5d5f84b362d45c30f9"
    }
    Frame {
        msec: 1424
        hash: "6206435ab4d05d5d5f84b362d45c30f9"
    }
    Frame {
        msec: 1440
        hash: "b0eb92df767e7cb61cc69d7363041263"
    }
    Frame {
        msec: 1456
        hash: "b0eb92df767e7cb61cc69d7363041263"
    }
    Frame {
        msec: 1472
        hash: "b0eb92df767e7cb61cc69d7363041263"
    }
    Frame {
        msec: 1488
        hash: "b0eb92df767e7cb61cc69d7363041263"
    }
    Frame {
        msec: 1504
        hash: "b0eb92df767e7cb61cc69d7363041263"
    }
    Frame {
        msec: 1520
        hash: "0306262c9594536e0eecf3d67e5910cf"
    }
    Frame {
        msec: 1536
        hash: "0306262c9594536e0eecf3d67e5910cf"
    }
    Frame {
        msec: 1552
        hash: "0306262c9594536e0eecf3d67e5910cf"
    }
    Frame {
        msec: 1568
        hash: "0306262c9594536e0eecf3d67e5910cf"
    }
    Frame {
        msec: 1584
        hash: "4f21a25c75cfabbcbd7c485c7c479bfc"
    }
    Frame {
        msec: 1600
        hash: "4f21a25c75cfabbcbd7c485c7c479bfc"
    }
    Frame {
        msec: 1616
        hash: "4f21a25c75cfabbcbd7c485c7c479bfc"
    }
    Frame {
        msec: 1632
        hash: "4f21a25c75cfabbcbd7c485c7c479bfc"
    }
    Frame {
        msec: 1648
        hash: "b627f215fdb6f62e6cbf2ddbe14dc794"
    }
    Frame {
        msec: 1664
        hash: "b627f215fdb6f62e6cbf2ddbe14dc794"
    }
    Frame {
        msec: 1680
        hash: "b627f215fdb6f62e6cbf2ddbe14dc794"
    }
    Frame {
        msec: 1696
        hash: "b627f215fdb6f62e6cbf2ddbe14dc794"
    }
    Frame {
        msec: 1712
        hash: "b627f215fdb6f62e6cbf2ddbe14dc794"
    }
    Frame {
        msec: 1728
        hash: "8c490b27882c58d34cbc941a0b10e6fe"
    }
    Frame {
        msec: 1744
        hash: "8c490b27882c58d34cbc941a0b10e6fe"
    }
    Key {
        type: 6
        key: 16777249
        modifiers: 0
        text: ""
        autorep: false
        count: 1
    }
    Frame {
        msec: 1760
        hash: "8c490b27882c58d34cbc941a0b10e6fe"
    }
    Frame {
        msec: 1776
        hash: "8c490b27882c58d34cbc941a0b10e6fe"
    }
    Frame {
        msec: 1792
        hash: "739abcde4a980e05932aa079245136cd"
    }
    Frame {
        msec: 1808
        hash: "739abcde4a980e05932aa079245136cd"
    }
    Frame {
        msec: 1824
        hash: "739abcde4a980e05932aa079245136cd"
    }
    Frame {
        msec: 1840
        hash: "739abcde4a980e05932aa079245136cd"
    }
    Frame {
        msec: 1856
        hash: "739abcde4a980e05932aa079245136cd"
    }
    Frame {
        msec: 1872
        hash: "af47b93ee81b0e4add42d9addad92219"
    }
    Frame {
        msec: 1888
        hash: "af47b93ee81b0e4add42d9addad92219"
    }
    Frame {
        msec: 1904
        hash: "af47b93ee81b0e4add42d9addad92219"
    }
    Frame {
        msec: 1920
        hash: "af47b93ee81b0e4add42d9addad92219"
    }
    Frame {
        msec: 1936
        image: "elide2.2.png"
    }
    Frame {
        msec: 1952
        hash: "43d7bee700464080f7535d398d60af25"
    }
    Frame {
        msec: 1968
        hash: "43d7bee700464080f7535d398d60af25"
    }
    Frame {
        msec: 1984
        hash: "43d7bee700464080f7535d398d60af25"
    }
    Frame {
        msec: 2000
        hash: "31bf9784a1e1b84cc0ed4a342284ce1a"
    }
    Frame {
        msec: 2016
        hash: "31bf9784a1e1b84cc0ed4a342284ce1a"
    }
    Frame {
        msec: 2032
        hash: "31bf9784a1e1b84cc0ed4a342284ce1a"
    }
    Frame {
        msec: 2048
        hash: "31bf9784a1e1b84cc0ed4a342284ce1a"
    }
    Frame {
        msec: 2064
        hash: "31bf9784a1e1b84cc0ed4a342284ce1a"
    }
    Frame {
        msec: 2080
        hash: "59db6dabc6c6930b9561bc906f49cc3c"
    }
    Frame {
        msec: 2096
        hash: "59db6dabc6c6930b9561bc906f49cc3c"
    }
    Frame {
        msec: 2112
        hash: "59db6dabc6c6930b9561bc906f49cc3c"
    }
    Frame {
        msec: 2128
        hash: "59db6dabc6c6930b9561bc906f49cc3c"
    }
    Frame {
        msec: 2144
        hash: "9e9cd1f4b9ad0980d6601e52c3d21402"
    }
    Frame {
        msec: 2160
        hash: "9e9cd1f4b9ad0980d6601e52c3d21402"
    }
    Frame {
        msec: 2176
        hash: "9e9cd1f4b9ad0980d6601e52c3d21402"
    }
    Frame {
        msec: 2192
        hash: "9e9cd1f4b9ad0980d6601e52c3d21402"
    }
    Frame {
        msec: 2208
        hash: "f8e23813215634224d2fb00f3d1993c5"
    }
    Frame {
        msec: 2224
        hash: "f8e23813215634224d2fb00f3d1993c5"
    }
    Frame {
        msec: 2240
        hash: "f8e23813215634224d2fb00f3d1993c5"
    }
    Frame {
        msec: 2256
        hash: "f8e23813215634224d2fb00f3d1993c5"
    }
    Frame {
        msec: 2272
        hash: "f8e23813215634224d2fb00f3d1993c5"
    }
    Frame {
        msec: 2288
        hash: "e867db62cb8ec10228ea7b2eceda3723"
    }
    Frame {
        msec: 2304
        hash: "e867db62cb8ec10228ea7b2eceda3723"
    }
    Frame {
        msec: 2320
        hash: "e867db62cb8ec10228ea7b2eceda3723"
    }
    Frame {
        msec: 2336
        hash: "e867db62cb8ec10228ea7b2eceda3723"
    }
    Frame {
        msec: 2352
        hash: "36da8cc7019fae8b1abc877961d3af41"
    }
    Frame {
        msec: 2368
        hash: "36da8cc7019fae8b1abc877961d3af41"
    }
    Frame {
        msec: 2384
        hash: "36da8cc7019fae8b1abc877961d3af41"
    }
    Frame {
        msec: 2400
        hash: "36da8cc7019fae8b1abc877961d3af41"
    }
    Frame {
        msec: 2416
        hash: "36da8cc7019fae8b1abc877961d3af41"
    }
    Frame {
        msec: 2432
        hash: "ea907beaf860fa21684fc524e876346c"
    }
    Frame {
        msec: 2448
        hash: "ea907beaf860fa21684fc524e876346c"
    }
    Frame {
        msec: 2464
        hash: "ea907beaf860fa21684fc524e876346c"
    }
    Frame {
        msec: 2480
        hash: "ea907beaf860fa21684fc524e876346c"
    }
    Frame {
        msec: 2496
        hash: "2b3eb80e842df2fa2b6c217a2948af45"
    }
    Frame {
        msec: 2512
        hash: "2b3eb80e842df2fa2b6c217a2948af45"
    }
    Frame {
        msec: 2528
        hash: "2b3eb80e842df2fa2b6c217a2948af45"
    }
    Frame {
        msec: 2544
        hash: "2b3eb80e842df2fa2b6c217a2948af45"
    }
    Frame {
        msec: 2560
        hash: "05ffb4d0af3fea65151596ea5b9b43c5"
    }
    Frame {
        msec: 2576
        hash: "05ffb4d0af3fea65151596ea5b9b43c5"
    }
    Frame {
        msec: 2592
        hash: "05ffb4d0af3fea65151596ea5b9b43c5"
    }
    Frame {
        msec: 2608
        hash: "05ffb4d0af3fea65151596ea5b9b43c5"
    }
    Frame {
        msec: 2624
        hash: "05ffb4d0af3fea65151596ea5b9b43c5"
    }
    Frame {
        msec: 2640
        hash: "612517436b6ef76f29b213944f742624"
    }
    Frame {
        msec: 2656
        hash: "612517436b6ef76f29b213944f742624"
    }
    Frame {
        msec: 2672
        hash: "612517436b6ef76f29b213944f742624"
    }
    Frame {
        msec: 2688
        hash: "612517436b6ef76f29b213944f742624"
    }
    Frame {
        msec: 2704
        hash: "a62c646572c94d55971445c0546e06fc"
    }
    Frame {
        msec: 2720
        hash: "a62c646572c94d55971445c0546e06fc"
    }
    Frame {
        msec: 2736
        hash: "a62c646572c94d55971445c0546e06fc"
    }
    Frame {
        msec: 2752
        hash: "a62c646572c94d55971445c0546e06fc"
    }
    Frame {
        msec: 2768
        hash: "91be655836fbf7f811a44ffa1e80b72a"
    }
    Frame {
        msec: 2784
        hash: "91be655836fbf7f811a44ffa1e80b72a"
    }
    Frame {
        msec: 2800
        hash: "91be655836fbf7f811a44ffa1e80b72a"
    }
    Frame {
        msec: 2816
        hash: "91be655836fbf7f811a44ffa1e80b72a"
    }
    Frame {
        msec: 2832
        hash: "91be655836fbf7f811a44ffa1e80b72a"
    }
    Frame {
        msec: 2848
        hash: "4fdf23d15633bd9dbcc1767fca797ef6"
    }
    Frame {
        msec: 2864
        hash: "4fdf23d15633bd9dbcc1767fca797ef6"
    }
    Frame {
        msec: 2880
        hash: "4fdf23d15633bd9dbcc1767fca797ef6"
    }
    Frame {
        msec: 2896
        image: "elide2.3.png"
    }
    Frame {
        msec: 2912
        hash: "a81f41ab4e100d92f643ae188c1a5b8a"
    }
    Frame {
        msec: 2928
        hash: "a81f41ab4e100d92f643ae188c1a5b8a"
    }
    Frame {
        msec: 2944
        hash: "a81f41ab4e100d92f643ae188c1a5b8a"
    }
    Frame {
        msec: 2960
        hash: "a81f41ab4e100d92f643ae188c1a5b8a"
    }
    Frame {
        msec: 2976
        hash: "a81f41ab4e100d92f643ae188c1a5b8a"
    }
    Frame {
        msec: 2992
        hash: "6785dbb1bd05081c5b5d890d4b4f28d5"
    }
    Frame {
        msec: 3008
        hash: "6785dbb1bd05081c5b5d890d4b4f28d5"
    }
    Frame {
        msec: 3024
        hash: "6785dbb1bd05081c5b5d890d4b4f28d5"
    }
    Frame {
        msec: 3040
        hash: "6785dbb1bd05081c5b5d890d4b4f28d5"
    }
    Frame {
        msec: 3056
        hash: "ca4fc26d93d4767ef7cdbac6b2e24cf5"
    }
    Frame {
        msec: 3072
        hash: "ca4fc26d93d4767ef7cdbac6b2e24cf5"
    }
    Frame {
        msec: 3088
        hash: "ca4fc26d93d4767ef7cdbac6b2e24cf5"
    }
    Frame {
        msec: 3104
        hash: "ca4fc26d93d4767ef7cdbac6b2e24cf5"
    }
    Frame {
        msec: 3120
        hash: "706fd39d5945f9f698e7fa6e26631b58"
    }
    Frame {
        msec: 3136
        hash: "706fd39d5945f9f698e7fa6e26631b58"
    }
    Frame {
        msec: 3152
        hash: "706fd39d5945f9f698e7fa6e26631b58"
    }
    Frame {
        msec: 3168
        hash: "706fd39d5945f9f698e7fa6e26631b58"
    }
    Frame {
        msec: 3184
        hash: "706fd39d5945f9f698e7fa6e26631b58"
    }
    Frame {
        msec: 3200
        hash: "c4ed351cacc86b5ca2c8198be0a754e0"
    }
    Frame {
        msec: 3216
        hash: "c4ed351cacc86b5ca2c8198be0a754e0"
    }
    Frame {
        msec: 3232
        hash: "c4ed351cacc86b5ca2c8198be0a754e0"
    }
    Frame {
        msec: 3248
        hash: "c4ed351cacc86b5ca2c8198be0a754e0"
    }
    Frame {
        msec: 3264
        hash: "addbbaca2d29fbc8c7907d51a8e9cdce"
    }
    Frame {
        msec: 3280
        hash: "addbbaca2d29fbc8c7907d51a8e9cdce"
    }
    Frame {
        msec: 3296
        hash: "addbbaca2d29fbc8c7907d51a8e9cdce"
    }
    Frame {
        msec: 3312
        hash: "addbbaca2d29fbc8c7907d51a8e9cdce"
    }
    Frame {
        msec: 3328
        hash: "fcb6b78276df1a6c839d6f30f8fe6495"
    }
    Frame {
        msec: 3344
        hash: "fcb6b78276df1a6c839d6f30f8fe6495"
    }
    Frame {
        msec: 3360
        hash: "fcb6b78276df1a6c839d6f30f8fe6495"
    }
    Frame {
        msec: 3376
        hash: "fcb6b78276df1a6c839d6f30f8fe6495"
    }
    Frame {
        msec: 3392
        hash: "fcb6b78276df1a6c839d6f30f8fe6495"
    }
    Frame {
        msec: 3408
        hash: "b066cbbb00a4bef4e730ea8131c2bbe5"
    }
    Frame {
        msec: 3424
        hash: "b066cbbb00a4bef4e730ea8131c2bbe5"
    }
    Frame {
        msec: 3440
        hash: "b066cbbb00a4bef4e730ea8131c2bbe5"
    }
    Frame {
        msec: 3456
        hash: "b066cbbb00a4bef4e730ea8131c2bbe5"
    }
    Frame {
        msec: 3472
        hash: "e6d801e738ed3265b0127b79da7e8ec5"
    }
    Frame {
        msec: 3488
        hash: "e6d801e738ed3265b0127b79da7e8ec5"
    }
    Frame {
        msec: 3504
        hash: "e6d801e738ed3265b0127b79da7e8ec5"
    }
    Frame {
        msec: 3520
        hash: "e6d801e738ed3265b0127b79da7e8ec5"
    }
    Frame {
        msec: 3536
        hash: "e6d801e738ed3265b0127b79da7e8ec5"
    }
    Frame {
        msec: 3552
        hash: "5b9a527ce399d0467b29c8813bbc7e6a"
    }
    Frame {
        msec: 3568
        hash: "5b9a527ce399d0467b29c8813bbc7e6a"
    }
    Frame {
        msec: 3584
        hash: "5b9a527ce399d0467b29c8813bbc7e6a"
    }
    Frame {
        msec: 3600
        hash: "5b9a527ce399d0467b29c8813bbc7e6a"
    }
    Frame {
        msec: 3616
        hash: "e9dd6c70c22d7b100a07ee837add697b"
    }
    Frame {
        msec: 3632
        hash: "e9dd6c70c22d7b100a07ee837add697b"
    }
    Frame {
        msec: 3648
        hash: "e9dd6c70c22d7b100a07ee837add697b"
    }
    Frame {
        msec: 3664
        hash: "e9dd6c70c22d7b100a07ee837add697b"
    }
    Frame {
        msec: 3680
        hash: "92e553a6e8385ceba6804075e5ed6add"
    }
    Frame {
        msec: 3696
        hash: "92e553a6e8385ceba6804075e5ed6add"
    }
    Frame {
        msec: 3712
        hash: "92e553a6e8385ceba6804075e5ed6add"
    }
    Frame {
        msec: 3728
        hash: "92e553a6e8385ceba6804075e5ed6add"
    }
    Frame {
        msec: 3744
        hash: "92e553a6e8385ceba6804075e5ed6add"
    }
    Frame {
        msec: 3760
        hash: "eafdc541e5bb2937cc472511758bd494"
    }
    Frame {
        msec: 3776
        hash: "eafdc541e5bb2937cc472511758bd494"
    }
    Frame {
        msec: 3792
        hash: "eafdc541e5bb2937cc472511758bd494"
    }
    Frame {
        msec: 3808
        hash: "eafdc541e5bb2937cc472511758bd494"
    }
    Frame {
        msec: 3824
        hash: "3d207efb5d563ec0a8640091710aa9fd"
    }
    Frame {
        msec: 3840
        hash: "3d207efb5d563ec0a8640091710aa9fd"
    }
    Frame {
        msec: 3856
        image: "elide2.4.png"
    }
    Frame {
        msec: 3872
        hash: "3d207efb5d563ec0a8640091710aa9fd"
    }
    Frame {
        msec: 3888
        hash: "d837a68f291b44c8ea4b92088ebccb2c"
    }
    Frame {
        msec: 3904
        hash: "d837a68f291b44c8ea4b92088ebccb2c"
    }
}
