import Qt.VisualTest 4.7

VisualTest {
    Frame {
        msec: 0
    }
    Frame {
        msec: 16
        image: "usingRepeater.0.png"
    }
    Frame {
        msec: 32
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 48
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 64
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 80
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 96
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 112
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 128
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 144
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 160
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 176
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 192
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 208
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 224
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 240
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 256
        hash: "1a396cf01a6c31155609532654653599"
    }
    Frame {
        msec: 272
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 288
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 304
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 320
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 336
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 352
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 368
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 384
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 400
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 416
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 432
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 448
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 464
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 480
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 496
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
    Frame {
        msec: 512
        hash: "8a4565aee33d40840bda26b65b6a0d90"
    }
}
